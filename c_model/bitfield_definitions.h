#define WORD_LENGTH 32

// VERSION_FIELD word description
#define VERSION_FIELD_bits             32
#define VERSION_FIELD_MAJOR_bits       8
#define VERSION_FIELD_MAJOR_msb        31
#define VERSION_FIELD_MAJOR_lsb        24
#define VERSION_FIELD_MAJOR_mf         1.
#define VERSION_FIELD_MAJOR_s2c        0
#define VERSION_FIELD_MAJOR_decb       0
#define VERSION_FIELD_MAJOR_apfix      0

#define VERSION_FIELD_MINOR_bits       8
#define VERSION_FIELD_MINOR_msb        23
#define VERSION_FIELD_MINOR_lsb        16
#define VERSION_FIELD_MINOR_mf         1.
#define VERSION_FIELD_MINOR_s2c        0
#define VERSION_FIELD_MINOR_decb       0
#define VERSION_FIELD_MINOR_apfix      0

#define VERSION_FIELD_PATCH_bits       16
#define VERSION_FIELD_PATCH_msb        15
#define VERSION_FIELD_PATCH_lsb        0
#define VERSION_FIELD_PATCH_mf         1.
#define VERSION_FIELD_PATCH_s2c        0
#define VERSION_FIELD_PATCH_decb       0
#define VERSION_FIELD_PATCH_apfix      0

#define VERSION 32

// M word description
#define M_bits                         65
#define M_W_bits                       64
#define M_W_msb                        64
#define M_W_lsb                        1
#define M_W_mf                         1.
#define M_W_s2c                        0
#define M_W_decb                       0
#define M_W_apfix                      0

// GenMetadata word description
#define GenMetadata_bits               64
#define GenMetadata_FLAG_bits          8
#define GenMetadata_FLAG_msb           63
#define GenMetadata_FLAG_lsb           56
#define GenMetadata_FLAG_mf            1.
#define GenMetadata_FLAG_s2c           0
#define GenMetadata_FLAG_decb          0
#define GenMetadata_FLAG_apfix         0

#define GenMetadata_OTHER_bits         56
#define GenMetadata_OTHER_msb          55
#define GenMetadata_OTHER_lsb          0
#define GenMetadata_OTHER_mf           1.
#define GenMetadata_OTHER_s2c          0
#define GenMetadata_OTHER_decb         0
#define GenMetadata_OTHER_apfix        0

// VERSION word description
#define VERSION_bits                   32
#define VERSION_FLAG_bits              8
#define VERSION_FLAG_msb               31
#define VERSION_FLAG_lsb               24
#define VERSION_FLAG_mf                1.
#define VERSION_FLAG_s2c               0
#define VERSION_FLAG_decb              0
#define VERSION_FLAG_apfix             0

#define VERSION_MAJOR_bits             8
#define VERSION_MAJOR_msb              23
#define VERSION_MAJOR_lsb              16
#define VERSION_MAJOR_mf               1.
#define VERSION_MAJOR_s2c              0
#define VERSION_MAJOR_decb             0
#define VERSION_MAJOR_apfix            0

#define VERSION_MINOR_bits             8
#define VERSION_MINOR_msb              15
#define VERSION_MINOR_lsb              8
#define VERSION_MINOR_mf               1.
#define VERSION_MINOR_s2c              0
#define VERSION_MINOR_decb             0
#define VERSION_MINOR_apfix            0

#define VERSION_SPARE_bits             8
#define VERSION_SPARE_msb              7
#define VERSION_SPARE_lsb              0
#define VERSION_SPARE_mf               1.
#define VERSION_SPARE_s2c              0
#define VERSION_SPARE_decb             0
#define VERSION_SPARE_apfix            0

#define EVT_HDR_LWORDS                 3

// EVT_HDR_W1 word description
#define EVT_HDR_W1_bits                64
#define EVT_HDR_W1_FLAG_bits           8
#define EVT_HDR_W1_FLAG_msb            63
#define EVT_HDR_W1_FLAG_lsb            56
#define EVT_HDR_W1_FLAG_mf             1.
#define EVT_HDR_W1_FLAG_s2c            0
#define EVT_HDR_W1_FLAG_decb           0
#define EVT_HDR_W1_FLAG_apfix          0
#define EVT_HDR_W1_FLAG_FLAG           171

#define EVT_HDR_W1_L0ID_bits           40
#define EVT_HDR_W1_L0ID_msb            55
#define EVT_HDR_W1_L0ID_lsb            16
#define EVT_HDR_W1_L0ID_mf             1.
#define EVT_HDR_W1_L0ID_s2c            0
#define EVT_HDR_W1_L0ID_decb           0
#define EVT_HDR_W1_L0ID_apfix          0

#define EVT_HDR_W1_BCID_bits           12
#define EVT_HDR_W1_BCID_msb            15
#define EVT_HDR_W1_BCID_lsb            4
#define EVT_HDR_W1_BCID_mf             1.
#define EVT_HDR_W1_BCID_s2c            0
#define EVT_HDR_W1_BCID_decb           0
#define EVT_HDR_W1_BCID_apfix          0

#define EVT_HDR_W1_SPARE_bits          4
#define EVT_HDR_W1_SPARE_msb           3
#define EVT_HDR_W1_SPARE_lsb           0
#define EVT_HDR_W1_SPARE_mf            1.
#define EVT_HDR_W1_SPARE_s2c           0
#define EVT_HDR_W1_SPARE_decb          0
#define EVT_HDR_W1_SPARE_apfix         0

// EVT_HDR_W2 word description
#define EVT_HDR_W2_bits                64
#define EVT_HDR_W2_RUNNUMBER_bits      32
#define EVT_HDR_W2_RUNNUMBER_msb       63
#define EVT_HDR_W2_RUNNUMBER_lsb       32
#define EVT_HDR_W2_RUNNUMBER_mf        1.
#define EVT_HDR_W2_RUNNUMBER_s2c       0
#define EVT_HDR_W2_RUNNUMBER_decb      0
#define EVT_HDR_W2_RUNNUMBER_apfix     0

#define EVT_HDR_W2_TIME_bits           32
#define EVT_HDR_W2_TIME_msb            31
#define EVT_HDR_W2_TIME_lsb            0
#define EVT_HDR_W2_TIME_mf             1.
#define EVT_HDR_W2_TIME_s2c            0
#define EVT_HDR_W2_TIME_decb           0
#define EVT_HDR_W2_TIME_apfix          0

// EVT_HDR_W3 word description
#define EVT_HDR_W3_bits                64
#define EVT_HDR_W3_STATUS_bits         32
#define EVT_HDR_W3_STATUS_msb          63
#define EVT_HDR_W3_STATUS_lsb          32
#define EVT_HDR_W3_STATUS_mf           1.
#define EVT_HDR_W3_STATUS_s2c          0
#define EVT_HDR_W3_STATUS_decb         0
#define EVT_HDR_W3_STATUS_apfix        0

#define EVT_HDR_W3_CRC_bits            32
#define EVT_HDR_W3_CRC_msb             31
#define EVT_HDR_W3_CRC_lsb             0
#define EVT_HDR_W3_CRC_mf              1.
#define EVT_HDR_W3_CRC_s2c             0
#define EVT_HDR_W3_CRC_decb            0
#define EVT_HDR_W3_CRC_apfix           0

#define EVT_FTR_LWORDS                 3

// EVT_FTR_W1 word description
#define EVT_FTR_W1_bits                64
#define EVT_FTR_W1_FLAG_bits           8
#define EVT_FTR_W1_FLAG_msb            63
#define EVT_FTR_W1_FLAG_lsb            56
#define EVT_FTR_W1_FLAG_mf             1.
#define EVT_FTR_W1_FLAG_s2c            0
#define EVT_FTR_W1_FLAG_decb           0
#define EVT_FTR_W1_FLAG_apfix          0
#define EVT_FTR_W1_FLAG_FLAG           205

#define EVT_FTR_W1_SPARE_bits          24
#define EVT_FTR_W1_SPARE_msb           55
#define EVT_FTR_W1_SPARE_lsb           32
#define EVT_FTR_W1_SPARE_mf            1.
#define EVT_FTR_W1_SPARE_s2c           0
#define EVT_FTR_W1_SPARE_decb          0
#define EVT_FTR_W1_SPARE_apfix         0

#define EVT_FTR_W1_HDR_CRC_bits        32
#define EVT_FTR_W1_HDR_CRC_msb         31
#define EVT_FTR_W1_HDR_CRC_lsb         0
#define EVT_FTR_W1_HDR_CRC_mf          1.
#define EVT_FTR_W1_HDR_CRC_s2c         0
#define EVT_FTR_W1_HDR_CRC_decb        0
#define EVT_FTR_W1_HDR_CRC_apfix       0

// EVT_FTR_W2 word description
#define EVT_FTR_W2_bits                64
#define EVT_FTR_W2_ERROR_FLAGS_bits    64
#define EVT_FTR_W2_ERROR_FLAGS_msb     63
#define EVT_FTR_W2_ERROR_FLAGS_lsb     0
#define EVT_FTR_W2_ERROR_FLAGS_mf      1.
#define EVT_FTR_W2_ERROR_FLAGS_s2c     0
#define EVT_FTR_W2_ERROR_FLAGS_decb    0
#define EVT_FTR_W2_ERROR_FLAGS_apfix   0

// EVT_FTR_W3 word description
#define EVT_FTR_W3_bits                64
#define EVT_FTR_W3_WORD_COUNT_bits     32
#define EVT_FTR_W3_WORD_COUNT_msb      63
#define EVT_FTR_W3_WORD_COUNT_lsb      32
#define EVT_FTR_W3_WORD_COUNT_mf       1.
#define EVT_FTR_W3_WORD_COUNT_s2c      0
#define EVT_FTR_W3_WORD_COUNT_decb     0
#define EVT_FTR_W3_WORD_COUNT_apfix    0

#define EVT_FTR_W3_CRC_bits            32
#define EVT_FTR_W3_CRC_msb             31
#define EVT_FTR_W3_CRC_lsb             0
#define EVT_FTR_W3_CRC_mf              1.
#define EVT_FTR_W3_CRC_s2c             0
#define EVT_FTR_W3_CRC_decb            0
#define EVT_FTR_W3_CRC_apfix           0

#define M_HDR_LWORDS                   1

// M_HDR_W1 word description
#define M_HDR_W1_bits                  64
#define M_HDR_W1_FLAG_bits             8
#define M_HDR_W1_FLAG_msb              63
#define M_HDR_W1_FLAG_lsb              56
#define M_HDR_W1_FLAG_mf               1.
#define M_HDR_W1_FLAG_s2c              0
#define M_HDR_W1_FLAG_decb             0
#define M_HDR_W1_FLAG_apfix            0
#define M_HDR_W1_FLAG_FLAG             85

#define M_HDR_W1_DET_bits              1
#define M_HDR_W1_DET_msb               55
#define M_HDR_W1_DET_lsb               55
#define M_HDR_W1_DET_mf                1.
#define M_HDR_W1_DET_s2c               0
#define M_HDR_W1_DET_decb              0
#define M_HDR_W1_DET_apfix             0
#define M_HDR_W1_DET_PIXEL             0
#define M_HDR_W1_DET_STRIP             1

#define M_HDR_W1_MODID_bits            18
#define M_HDR_W1_MODID_msb             54
#define M_HDR_W1_MODID_lsb             37
#define M_HDR_W1_MODID_mf              1.
#define M_HDR_W1_MODID_s2c             0
#define M_HDR_W1_MODID_decb            0
#define M_HDR_W1_MODID_apfix           0

#define M_HDR_W1_MODULE_TYPE_bits      4
#define M_HDR_W1_MODULE_TYPE_msb       36
#define M_HDR_W1_MODULE_TYPE_lsb       33
#define M_HDR_W1_MODULE_TYPE_mf        1.
#define M_HDR_W1_MODULE_TYPE_s2c       0
#define M_HDR_W1_MODULE_TYPE_decb      0
#define M_HDR_W1_MODULE_TYPE_apfix     0

#define M_HDR_W1_PHI_INDEX_bits        12
#define M_HDR_W1_PHI_INDEX_msb         32
#define M_HDR_W1_PHI_INDEX_lsb         21
#define M_HDR_W1_PHI_INDEX_mf          1.
#define M_HDR_W1_PHI_INDEX_s2c         0
#define M_HDR_W1_PHI_INDEX_decb        0
#define M_HDR_W1_PHI_INDEX_apfix       0

#define M_HDR_W1_ETA_INDEX_bits        12
#define M_HDR_W1_ETA_INDEX_msb         20
#define M_HDR_W1_ETA_INDEX_lsb         9
#define M_HDR_W1_ETA_INDEX_mf          1.
#define M_HDR_W1_ETA_INDEX_s2c         0
#define M_HDR_W1_ETA_INDEX_decb        0
#define M_HDR_W1_ETA_INDEX_apfix       0

#define M_HDR_W1_LAYER_bits            4
#define M_HDR_W1_LAYER_msb             8
#define M_HDR_W1_LAYER_lsb             5
#define M_HDR_W1_LAYER_mf              1.
#define M_HDR_W1_LAYER_s2c             0
#define M_HDR_W1_LAYER_decb            0
#define M_HDR_W1_LAYER_apfix           0

#define M_HDR_W1_SPARE_bits            5
#define M_HDR_W1_SPARE_msb             4
#define M_HDR_W1_SPARE_lsb             0
#define M_HDR_W1_SPARE_mf              1.
#define M_HDR_W1_SPARE_s2c             0
#define M_HDR_W1_SPARE_decb            0
#define M_HDR_W1_SPARE_apfix           0

#define RD_HDR_LWORDS                  2

// RD_HDR_W1 word description
#define RD_HDR_W1_bits                 64
#define RD_HDR_W1_FLAG_bits            8
#define RD_HDR_W1_FLAG_msb             63
#define RD_HDR_W1_FLAG_lsb             56
#define RD_HDR_W1_FLAG_mf              1.
#define RD_HDR_W1_FLAG_s2c             0
#define RD_HDR_W1_FLAG_decb            0
#define RD_HDR_W1_FLAG_apfix           0

#define RD_HDR_W1_TYPE_bits            4
#define RD_HDR_W1_TYPE_msb             55
#define RD_HDR_W1_TYPE_lsb             52
#define RD_HDR_W1_TYPE_mf              1.
#define RD_HDR_W1_TYPE_s2c             0
#define RD_HDR_W1_TYPE_decb            0
#define RD_HDR_W1_TYPE_apfix           0

#define RD_HDR_W1_ETA_REGION_bits      6
#define RD_HDR_W1_ETA_REGION_msb       51
#define RD_HDR_W1_ETA_REGION_lsb       46
#define RD_HDR_W1_ETA_REGION_mf        1.
#define RD_HDR_W1_ETA_REGION_s2c       0
#define RD_HDR_W1_ETA_REGION_decb      0
#define RD_HDR_W1_ETA_REGION_apfix     0

#define RD_HDR_W1_PHI_REGION_bits      6
#define RD_HDR_W1_PHI_REGION_msb       45
#define RD_HDR_W1_PHI_REGION_lsb       40
#define RD_HDR_W1_PHI_REGION_mf        1.
#define RD_HDR_W1_PHI_REGION_s2c       0
#define RD_HDR_W1_PHI_REGION_decb      0
#define RD_HDR_W1_PHI_REGION_apfix     0

#define RD_HDR_W1_SLICE_bits           5
#define RD_HDR_W1_SLICE_msb            39
#define RD_HDR_W1_SLICE_lsb            35
#define RD_HDR_W1_SLICE_mf             1.
#define RD_HDR_W1_SLICE_s2c            0
#define RD_HDR_W1_SLICE_decb           0
#define RD_HDR_W1_SLICE_apfix          0

#define RD_HDR_W1_HOUGH_X_BIN_bits     8
#define RD_HDR_W1_HOUGH_X_BIN_msb      34
#define RD_HDR_W1_HOUGH_X_BIN_lsb      27
#define RD_HDR_W1_HOUGH_X_BIN_mf       1.
#define RD_HDR_W1_HOUGH_X_BIN_s2c      0
#define RD_HDR_W1_HOUGH_X_BIN_decb     0
#define RD_HDR_W1_HOUGH_X_BIN_apfix    0

#define RD_HDR_W1_HOUGH_Y_BIN_bits     8
#define RD_HDR_W1_HOUGH_Y_BIN_msb      26
#define RD_HDR_W1_HOUGH_Y_BIN_lsb      19
#define RD_HDR_W1_HOUGH_Y_BIN_mf       1.
#define RD_HDR_W1_HOUGH_Y_BIN_s2c      0
#define RD_HDR_W1_HOUGH_Y_BIN_decb     0
#define RD_HDR_W1_HOUGH_Y_BIN_apfix    0

#define RD_HDR_W1_SECOND_STAGE_bits    1
#define RD_HDR_W1_SECOND_STAGE_msb     18
#define RD_HDR_W1_SECOND_STAGE_lsb     18
#define RD_HDR_W1_SECOND_STAGE_mf      1.
#define RD_HDR_W1_SECOND_STAGE_s2c     0
#define RD_HDR_W1_SECOND_STAGE_decb    0
#define RD_HDR_W1_SECOND_STAGE_apfix   0

#define RD_HDR_W1_LAYER_BITMASK_bits   13
#define RD_HDR_W1_LAYER_BITMASK_msb    17
#define RD_HDR_W1_LAYER_BITMASK_lsb    5
#define RD_HDR_W1_LAYER_BITMASK_mf     1.
#define RD_HDR_W1_LAYER_BITMASK_s2c    0
#define RD_HDR_W1_LAYER_BITMASK_decb   0
#define RD_HDR_W1_LAYER_BITMASK_apfix  0

#define RD_HDR_W1_SPARE_bits           5
#define RD_HDR_W1_SPARE_msb            4
#define RD_HDR_W1_SPARE_lsb            0
#define RD_HDR_W1_SPARE_mf             1.
#define RD_HDR_W1_SPARE_s2c            0
#define RD_HDR_W1_SPARE_decb           0
#define RD_HDR_W1_SPARE_apfix          0

// RD_HDR_W2 word description
#define RD_HDR_W2_bits                 64
#define RD_HDR_W2_GLOBAL_PHI_bits      16
#define RD_HDR_W2_GLOBAL_PHI_msb       63
#define RD_HDR_W2_GLOBAL_PHI_lsb       48
#define RD_HDR_W2_GLOBAL_PHI_mf        1.
#define RD_HDR_W2_GLOBAL_PHI_s2c       0
#define RD_HDR_W2_GLOBAL_PHI_decb      0
#define RD_HDR_W2_GLOBAL_PHI_apfix     0

#define RD_HDR_W2_GLOBAL_ETA_bits      16
#define RD_HDR_W2_GLOBAL_ETA_msb       47
#define RD_HDR_W2_GLOBAL_ETA_lsb       32
#define RD_HDR_W2_GLOBAL_ETA_mf        1.
#define RD_HDR_W2_GLOBAL_ETA_s2c       0
#define RD_HDR_W2_GLOBAL_ETA_decb      0
#define RD_HDR_W2_GLOBAL_ETA_apfix     0

#define RD_HDR_W2_SPARE_bits           32
#define RD_HDR_W2_SPARE_msb            31
#define RD_HDR_W2_SPARE_lsb            0
#define RD_HDR_W2_SPARE_mf             1.
#define RD_HDR_W2_SPARE_s2c            0
#define RD_HDR_W2_SPARE_decb           0
#define RD_HDR_W2_SPARE_apfix          0

#define GTRACK_HDR_LWORDS              3

// GTRACK_HDR_W1 word description
#define GTRACK_HDR_W1_bits             64
#define GTRACK_HDR_W1_FLAG_bits        8
#define GTRACK_HDR_W1_FLAG_msb         63
#define GTRACK_HDR_W1_FLAG_lsb         56
#define GTRACK_HDR_W1_FLAG_mf          1.
#define GTRACK_HDR_W1_FLAG_s2c         0
#define GTRACK_HDR_W1_FLAG_decb        0
#define GTRACK_HDR_W1_FLAG_apfix       0
#define GTRACK_HDR_W1_FLAG_FLAG        238

#define GTRACK_HDR_W1_TYPE_bits        4
#define GTRACK_HDR_W1_TYPE_msb         55
#define GTRACK_HDR_W1_TYPE_lsb         52
#define GTRACK_HDR_W1_TYPE_mf          1.
#define GTRACK_HDR_W1_TYPE_s2c         0
#define GTRACK_HDR_W1_TYPE_decb        0
#define GTRACK_HDR_W1_TYPE_apfix       0

#define GTRACK_HDR_W1_ETA_REGION_bits  5
#define GTRACK_HDR_W1_ETA_REGION_msb   51
#define GTRACK_HDR_W1_ETA_REGION_lsb   47
#define GTRACK_HDR_W1_ETA_REGION_mf    1.
#define GTRACK_HDR_W1_ETA_REGION_s2c   0
#define GTRACK_HDR_W1_ETA_REGION_decb  0
#define GTRACK_HDR_W1_ETA_REGION_apfix 0

#define GTRACK_HDR_W1_PHI_REGION_bits  5
#define GTRACK_HDR_W1_PHI_REGION_msb   46
#define GTRACK_HDR_W1_PHI_REGION_lsb   42
#define GTRACK_HDR_W1_PHI_REGION_mf    1.
#define GTRACK_HDR_W1_PHI_REGION_s2c   0
#define GTRACK_HDR_W1_PHI_REGION_decb  0
#define GTRACK_HDR_W1_PHI_REGION_apfix 0

#define GTRACK_HDR_W1_SLICE_bits       5
#define GTRACK_HDR_W1_SLICE_msb        41
#define GTRACK_HDR_W1_SLICE_lsb        37
#define GTRACK_HDR_W1_SLICE_mf         1.
#define GTRACK_HDR_W1_SLICE_s2c        0
#define GTRACK_HDR_W1_SLICE_decb       0
#define GTRACK_HDR_W1_SLICE_apfix      0

#define GTRACK_HDR_W1_HOUGH_X_BIN_bits 8
#define GTRACK_HDR_W1_HOUGH_X_BIN_msb  36
#define GTRACK_HDR_W1_HOUGH_X_BIN_lsb  29
#define GTRACK_HDR_W1_HOUGH_X_BIN_mf   1.
#define GTRACK_HDR_W1_HOUGH_X_BIN_s2c  0
#define GTRACK_HDR_W1_HOUGH_X_BIN_decb 0
#define GTRACK_HDR_W1_HOUGH_X_BIN_apfix 0

#define GTRACK_HDR_W1_HOUGH_Y_BIN_bits 8
#define GTRACK_HDR_W1_HOUGH_Y_BIN_msb  28
#define GTRACK_HDR_W1_HOUGH_Y_BIN_lsb  21
#define GTRACK_HDR_W1_HOUGH_Y_BIN_mf   1.
#define GTRACK_HDR_W1_HOUGH_Y_BIN_s2c  0
#define GTRACK_HDR_W1_HOUGH_Y_BIN_decb 0
#define GTRACK_HDR_W1_HOUGH_Y_BIN_apfix 0

#define GTRACK_HDR_W1_SECOND_STAGE_bits 1
#define GTRACK_HDR_W1_SECOND_STAGE_msb 20
#define GTRACK_HDR_W1_SECOND_STAGE_lsb 20
#define GTRACK_HDR_W1_SECOND_STAGE_mf  1.
#define GTRACK_HDR_W1_SECOND_STAGE_s2c 0
#define GTRACK_HDR_W1_SECOND_STAGE_decb 0
#define GTRACK_HDR_W1_SECOND_STAGE_apfix 0

#define GTRACK_HDR_W1_LAYER_BITMASK_bits 13
#define GTRACK_HDR_W1_LAYER_BITMASK_msb 19
#define GTRACK_HDR_W1_LAYER_BITMASK_lsb 7
#define GTRACK_HDR_W1_LAYER_BITMASK_mf 1.
#define GTRACK_HDR_W1_LAYER_BITMASK_s2c 0
#define GTRACK_HDR_W1_LAYER_BITMASK_decb 0
#define GTRACK_HDR_W1_LAYER_BITMASK_apfix 0

#define GTRACK_HDR_W1_SPARE_bits       7
#define GTRACK_HDR_W1_SPARE_msb        6
#define GTRACK_HDR_W1_SPARE_lsb        0
#define GTRACK_HDR_W1_SPARE_mf         1.
#define GTRACK_HDR_W1_SPARE_s2c        0
#define GTRACK_HDR_W1_SPARE_decb       0
#define GTRACK_HDR_W1_SPARE_apfix      0

// GTRACK_HDR_W2 word description
#define GTRACK_HDR_W2_bits             64
#define GTRACK_HDR_W2_SCORE_bits       16
#define GTRACK_HDR_W2_SCORE_msb        63
#define GTRACK_HDR_W2_SCORE_lsb        48
#define GTRACK_HDR_W2_SCORE_mf         2048.
#define GTRACK_HDR_W2_SCORE_s2c        0
#define GTRACK_HDR_W2_SCORE_decb       0
#define GTRACK_HDR_W2_SCORE_apfix      0

#define GTRACK_HDR_W2_D0_bits          16
#define GTRACK_HDR_W2_D0_msb           47
#define GTRACK_HDR_W2_D0_lsb           32
#define GTRACK_HDR_W2_D0_mf            4096.
#define GTRACK_HDR_W2_D0_s2c           1
#define GTRACK_HDR_W2_D0_decb          0
#define GTRACK_HDR_W2_D0_apfix         0

#define GTRACK_HDR_W2_Z0_bits          16
#define GTRACK_HDR_W2_Z0_msb           31
#define GTRACK_HDR_W2_Z0_lsb           16
#define GTRACK_HDR_W2_Z0_mf            32.
#define GTRACK_HDR_W2_Z0_s2c           1
#define GTRACK_HDR_W2_Z0_decb          0
#define GTRACK_HDR_W2_Z0_apfix         0

#define GTRACK_HDR_W2_SPARE_bits       16
#define GTRACK_HDR_W2_SPARE_msb        15
#define GTRACK_HDR_W2_SPARE_lsb        0
#define GTRACK_HDR_W2_SPARE_mf         1.
#define GTRACK_HDR_W2_SPARE_s2c        0
#define GTRACK_HDR_W2_SPARE_decb       0
#define GTRACK_HDR_W2_SPARE_apfix      0

// GTRACK_HDR_W3 word description
#define GTRACK_HDR_W3_bits             64
#define GTRACK_HDR_W3_QOVERPT_bits     16
#define GTRACK_HDR_W3_QOVERPT_msb      63
#define GTRACK_HDR_W3_QOVERPT_lsb      48
#define GTRACK_HDR_W3_QOVERPT_mf       32768.
#define GTRACK_HDR_W3_QOVERPT_s2c      1
#define GTRACK_HDR_W3_QOVERPT_decb     0
#define GTRACK_HDR_W3_QOVERPT_apfix    0

#define GTRACK_HDR_W3_PHI_bits         16
#define GTRACK_HDR_W3_PHI_msb          47
#define GTRACK_HDR_W3_PHI_lsb          32
#define GTRACK_HDR_W3_PHI_mf           8192.
#define GTRACK_HDR_W3_PHI_s2c          1
#define GTRACK_HDR_W3_PHI_decb         0
#define GTRACK_HDR_W3_PHI_apfix        0

#define GTRACK_HDR_W3_ETA_bits         16
#define GTRACK_HDR_W3_ETA_msb          31
#define GTRACK_HDR_W3_ETA_lsb          16
#define GTRACK_HDR_W3_ETA_mf           8192.
#define GTRACK_HDR_W3_ETA_s2c          1
#define GTRACK_HDR_W3_ETA_decb         0
#define GTRACK_HDR_W3_ETA_apfix        0

#define GTRACK_HDR_W3_SPARE_bits       16
#define GTRACK_HDR_W3_SPARE_msb        15
#define GTRACK_HDR_W3_SPARE_lsb        0
#define GTRACK_HDR_W3_SPARE_mf         1.
#define GTRACK_HDR_W3_SPARE_s2c        0
#define GTRACK_HDR_W3_SPARE_decb       0
#define GTRACK_HDR_W3_SPARE_apfix      0

// PIXEL_CLUSTER word description
#define PIXEL_CLUSTER_bits             64
#define PIXEL_CLUSTER_LAST_bits        1
#define PIXEL_CLUSTER_LAST_msb         63
#define PIXEL_CLUSTER_LAST_lsb         63
#define PIXEL_CLUSTER_LAST_mf          1.
#define PIXEL_CLUSTER_LAST_s2c         0
#define PIXEL_CLUSTER_LAST_decb        0
#define PIXEL_CLUSTER_LAST_apfix       0

#define PIXEL_CLUSTER_COL_SIZE_bits    2
#define PIXEL_CLUSTER_COL_SIZE_msb     62
#define PIXEL_CLUSTER_COL_SIZE_lsb     61
#define PIXEL_CLUSTER_COL_SIZE_mf      1.
#define PIXEL_CLUSTER_COL_SIZE_s2c     0
#define PIXEL_CLUSTER_COL_SIZE_decb    0
#define PIXEL_CLUSTER_COL_SIZE_apfix   0

#define PIXEL_CLUSTER_COL_bits         13
#define PIXEL_CLUSTER_COL_msb          60
#define PIXEL_CLUSTER_COL_lsb          48
#define PIXEL_CLUSTER_COL_mf           1.
#define PIXEL_CLUSTER_COL_s2c          0
#define PIXEL_CLUSTER_COL_decb         0
#define PIXEL_CLUSTER_COL_apfix        0

#define PIXEL_CLUSTER_ROW_SIZE_bits    3
#define PIXEL_CLUSTER_ROW_SIZE_msb     47
#define PIXEL_CLUSTER_ROW_SIZE_lsb     45
#define PIXEL_CLUSTER_ROW_SIZE_mf      1.
#define PIXEL_CLUSTER_ROW_SIZE_s2c     0
#define PIXEL_CLUSTER_ROW_SIZE_decb    0
#define PIXEL_CLUSTER_ROW_SIZE_apfix   0

#define PIXEL_CLUSTER_ROW_bits         13
#define PIXEL_CLUSTER_ROW_msb          44
#define PIXEL_CLUSTER_ROW_lsb          32
#define PIXEL_CLUSTER_ROW_mf           1.
#define PIXEL_CLUSTER_ROW_s2c          0
#define PIXEL_CLUSTER_ROW_decb         0
#define PIXEL_CLUSTER_ROW_apfix        0

#define PIXEL_CLUSTER_CLUSTERID_bits   13
#define PIXEL_CLUSTER_CLUSTERID_msb    31
#define PIXEL_CLUSTER_CLUSTERID_lsb    19
#define PIXEL_CLUSTER_CLUSTERID_mf     1.
#define PIXEL_CLUSTER_CLUSTERID_s2c    0
#define PIXEL_CLUSTER_CLUSTERID_decb   0
#define PIXEL_CLUSTER_CLUSTERID_apfix  0

#define PIXEL_CLUSTER_SPARE_bits       19
#define PIXEL_CLUSTER_SPARE_msb        18
#define PIXEL_CLUSTER_SPARE_lsb        0
#define PIXEL_CLUSTER_SPARE_mf         1.
#define PIXEL_CLUSTER_SPARE_s2c        0
#define PIXEL_CLUSTER_SPARE_decb       0
#define PIXEL_CLUSTER_SPARE_apfix      0

// STRIP_CLUSTER word description
#define STRIP_CLUSTER_bits             32
#define STRIP_CLUSTER_LAST_bits        1
#define STRIP_CLUSTER_LAST_msb         31
#define STRIP_CLUSTER_LAST_lsb         31
#define STRIP_CLUSTER_LAST_mf          1.
#define STRIP_CLUSTER_LAST_s2c         0
#define STRIP_CLUSTER_LAST_decb        0
#define STRIP_CLUSTER_LAST_apfix       0

#define STRIP_CLUSTER_ROW_bits         1
#define STRIP_CLUSTER_ROW_msb          30
#define STRIP_CLUSTER_ROW_lsb          30
#define STRIP_CLUSTER_ROW_mf           1.
#define STRIP_CLUSTER_ROW_s2c          0
#define STRIP_CLUSTER_ROW_decb         0
#define STRIP_CLUSTER_ROW_apfix        0

#define STRIP_CLUSTER_NSTRIPS_bits     2
#define STRIP_CLUSTER_NSTRIPS_msb      29
#define STRIP_CLUSTER_NSTRIPS_lsb      28
#define STRIP_CLUSTER_NSTRIPS_mf       1.
#define STRIP_CLUSTER_NSTRIPS_s2c      0
#define STRIP_CLUSTER_NSTRIPS_decb     0
#define STRIP_CLUSTER_NSTRIPS_apfix    0

#define STRIP_CLUSTER_STRIP_INDEX_bits 12
#define STRIP_CLUSTER_STRIP_INDEX_msb  27
#define STRIP_CLUSTER_STRIP_INDEX_lsb  16
#define STRIP_CLUSTER_STRIP_INDEX_mf   1.
#define STRIP_CLUSTER_STRIP_INDEX_s2c  0
#define STRIP_CLUSTER_STRIP_INDEX_decb 0
#define STRIP_CLUSTER_STRIP_INDEX_apfix 0

#define STRIP_CLUSTER_CLUSTERID_bits   13
#define STRIP_CLUSTER_CLUSTERID_msb    15
#define STRIP_CLUSTER_CLUSTERID_lsb    3
#define STRIP_CLUSTER_CLUSTERID_mf     1.
#define STRIP_CLUSTER_CLUSTERID_s2c    0
#define STRIP_CLUSTER_CLUSTERID_decb   0
#define STRIP_CLUSTER_CLUSTERID_apfix  0

#define STRIP_CLUSTER_SPARE_bits       3
#define STRIP_CLUSTER_SPARE_msb        2
#define STRIP_CLUSTER_SPARE_lsb        0
#define STRIP_CLUSTER_SPARE_mf         1.
#define STRIP_CLUSTER_SPARE_s2c        0
#define STRIP_CLUSTER_SPARE_decb       0
#define STRIP_CLUSTER_SPARE_apfix      0

#define GHITZ_LWORDS                   2

// GHITZ_W1 word description
#define GHITZ_W1_bits                  64
#define GHITZ_W1_LAST_bits             1
#define GHITZ_W1_LAST_msb              63
#define GHITZ_W1_LAST_lsb              63
#define GHITZ_W1_LAST_mf               1.
#define GHITZ_W1_LAST_s2c              0
#define GHITZ_W1_LAST_decb             1
#define GHITZ_W1_LAST_apfix            0

#define GHITZ_W1_LYR_bits              4
#define GHITZ_W1_LYR_msb               62
#define GHITZ_W1_LYR_lsb               59
#define GHITZ_W1_LYR_mf                1.
#define GHITZ_W1_LYR_s2c               0
#define GHITZ_W1_LYR_decb              4
#define GHITZ_W1_LYR_apfix             0

#define GHITZ_W1_RAD_bits              16
#define GHITZ_W1_RAD_msb               58
#define GHITZ_W1_RAD_lsb               43
#define GHITZ_W1_RAD_mf                64.
#define GHITZ_W1_RAD_s2c               0
#define GHITZ_W1_RAD_decb              10
#define GHITZ_W1_RAD_apfix             6

#define GHITZ_W1_PHI_bits              16
#define GHITZ_W1_PHI_msb               42
#define GHITZ_W1_PHI_lsb               27
#define GHITZ_W1_PHI_mf                8192.
#define GHITZ_W1_PHI_s2c               1
#define GHITZ_W1_PHI_decb              3
#define GHITZ_W1_PHI_apfix             13

#define GHITZ_W1_Z_bits                16
#define GHITZ_W1_Z_msb                 26
#define GHITZ_W1_Z_lsb                 11
#define GHITZ_W1_Z_mf                  32.
#define GHITZ_W1_Z_s2c                 1
#define GHITZ_W1_Z_decb                11
#define GHITZ_W1_Z_apfix               5

#define GHITZ_W1_ROW_bits              6
#define GHITZ_W1_ROW_msb               10
#define GHITZ_W1_ROW_lsb               5
#define GHITZ_W1_ROW_mf                1.
#define GHITZ_W1_ROW_s2c               0
#define GHITZ_W1_ROW_decb              6
#define GHITZ_W1_ROW_apfix             0

#define GHITZ_W1_SPARE_bits            5
#define GHITZ_W1_SPARE_msb             4
#define GHITZ_W1_SPARE_lsb             0
#define GHITZ_W1_SPARE_mf              1.
#define GHITZ_W1_SPARE_s2c             0
#define GHITZ_W1_SPARE_decb            5
#define GHITZ_W1_SPARE_apfix           0

// GHITZ_W2 word description
#define GHITZ_W2_bits                  64
#define GHITZ_W2_CLUSTER1_bits         13
#define GHITZ_W2_CLUSTER1_msb          63
#define GHITZ_W2_CLUSTER1_lsb          51
#define GHITZ_W2_CLUSTER1_mf           1.
#define GHITZ_W2_CLUSTER1_s2c          0
#define GHITZ_W2_CLUSTER1_decb         0
#define GHITZ_W2_CLUSTER1_apfix        0

#define GHITZ_W2_CLUSTER2_bits         13
#define GHITZ_W2_CLUSTER2_msb          50
#define GHITZ_W2_CLUSTER2_lsb          38
#define GHITZ_W2_CLUSTER2_mf           1.
#define GHITZ_W2_CLUSTER2_s2c          0
#define GHITZ_W2_CLUSTER2_decb         0
#define GHITZ_W2_CLUSTER2_apfix        0

#define GHITZ_W2_SPARE_bits            38
#define GHITZ_W2_SPARE_msb             37
#define GHITZ_W2_SPARE_lsb             0
#define GHITZ_W2_SPARE_mf              1.
#define GHITZ_W2_SPARE_s2c             0
#define GHITZ_W2_SPARE_decb            0
#define GHITZ_W2_SPARE_apfix           0

// EDM_STRIP_CLUSTER word description
#define EDM_STRIP_CLUSTER_bits         13
#define EDM_STRIP_CLUSTER_LAST_bits    1
#define EDM_STRIP_CLUSTER_LAST_msb     12
#define EDM_STRIP_CLUSTER_LAST_lsb     12
#define EDM_STRIP_CLUSTER_LAST_mf      1.
#define EDM_STRIP_CLUSTER_LAST_s2c     0
#define EDM_STRIP_CLUSTER_LAST_decb    0
#define EDM_STRIP_CLUSTER_LAST_apfix   0

#define EDM_STRIP_CLUSTER_ROW_bits     1
#define EDM_STRIP_CLUSTER_ROW_msb      11
#define EDM_STRIP_CLUSTER_ROW_lsb      11
#define EDM_STRIP_CLUSTER_ROW_mf       1.
#define EDM_STRIP_CLUSTER_ROW_s2c      0
#define EDM_STRIP_CLUSTER_ROW_decb     0
#define EDM_STRIP_CLUSTER_ROW_apfix    0

#define EDM_STRIP_CLUSTER_NSTRIPS_bits 1
#define EDM_STRIP_CLUSTER_NSTRIPS_msb  10
#define EDM_STRIP_CLUSTER_NSTRIPS_lsb  10
#define EDM_STRIP_CLUSTER_NSTRIPS_mf   1.
#define EDM_STRIP_CLUSTER_NSTRIPS_s2c  0
#define EDM_STRIP_CLUSTER_NSTRIPS_decb 0
#define EDM_STRIP_CLUSTER_NSTRIPS_apfix 0

#define EDM_STRIP_CLUSTER_STRIP_INDEX_bits 1
#define EDM_STRIP_CLUSTER_STRIP_INDEX_msb 9
#define EDM_STRIP_CLUSTER_STRIP_INDEX_lsb 9
#define EDM_STRIP_CLUSTER_STRIP_INDEX_mf 1.
#define EDM_STRIP_CLUSTER_STRIP_INDEX_s2c 0
#define EDM_STRIP_CLUSTER_STRIP_INDEX_decb 0
#define EDM_STRIP_CLUSTER_STRIP_INDEX_apfix 0

#define EDM_STRIP_CLUSTER_NCLUSTERS_bits 1
#define EDM_STRIP_CLUSTER_NCLUSTERS_msb 8
#define EDM_STRIP_CLUSTER_NCLUSTERS_lsb 8
#define EDM_STRIP_CLUSTER_NCLUSTERS_mf 1.
#define EDM_STRIP_CLUSTER_NCLUSTERS_s2c 0
#define EDM_STRIP_CLUSTER_NCLUSTERS_decb 0
#define EDM_STRIP_CLUSTER_NCLUSTERS_apfix 0

#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_bits 1
#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_msb 7
#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_lsb 7
#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_mf 1.
#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_s2c 0
#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_decb 0
#define EDM_STRIP_CLUSTER_CLUSTERIDHASH_apfix 0

#define EDM_STRIP_CLUSTER_CLUSTERID_bits 1
#define EDM_STRIP_CLUSTER_CLUSTERID_msb 6
#define EDM_STRIP_CLUSTER_CLUSTERID_lsb 6
#define EDM_STRIP_CLUSTER_CLUSTERID_mf 1.
#define EDM_STRIP_CLUSTER_CLUSTERID_s2c 0
#define EDM_STRIP_CLUSTER_CLUSTERID_decb 0
#define EDM_STRIP_CLUSTER_CLUSTERID_apfix 0

#define EDM_STRIP_CLUSTER_LOCALPOSITION_bits 1
#define EDM_STRIP_CLUSTER_LOCALPOSITION_msb 5
#define EDM_STRIP_CLUSTER_LOCALPOSITION_lsb 5
#define EDM_STRIP_CLUSTER_LOCALPOSITION_mf 1.
#define EDM_STRIP_CLUSTER_LOCALPOSITION_s2c 0
#define EDM_STRIP_CLUSTER_LOCALPOSITION_decb 0
#define EDM_STRIP_CLUSTER_LOCALPOSITION_apfix 0

#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_bits 1
#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_msb 4
#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_lsb 4
#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_mf 1.
#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_s2c 0
#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_decb 0
#define EDM_STRIP_CLUSTER_LOCALCOVARIANCE_apfix 0

#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_bits 1
#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_msb 3
#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_lsb 3
#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_mf 1.
#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_s2c 0
#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_decb 0
#define EDM_STRIP_CLUSTER_GLOBAL_POSITION_apfix 0

#define EDM_STRIP_CLUSTER_NSTRIP_bits  1
#define EDM_STRIP_CLUSTER_NSTRIP_msb   2
#define EDM_STRIP_CLUSTER_NSTRIP_lsb   2
#define EDM_STRIP_CLUSTER_NSTRIP_mf    1.
#define EDM_STRIP_CLUSTER_NSTRIP_s2c   0
#define EDM_STRIP_CLUSTER_NSTRIP_decb  0
#define EDM_STRIP_CLUSTER_NSTRIP_apfix 0

#define EDM_STRIP_CLUSTER_STRIPID_bits 1
#define EDM_STRIP_CLUSTER_STRIPID_msb  1
#define EDM_STRIP_CLUSTER_STRIPID_lsb  1
#define EDM_STRIP_CLUSTER_STRIPID_mf   1.
#define EDM_STRIP_CLUSTER_STRIPID_s2c  0
#define EDM_STRIP_CLUSTER_STRIPID_decb 0
#define EDM_STRIP_CLUSTER_STRIPID_apfix 0

#define EDM_STRIP_CLUSTER_CHANNELSPHI_bits 1
#define EDM_STRIP_CLUSTER_CHANNELSPHI_msb 0
#define EDM_STRIP_CLUSTER_CHANNELSPHI_lsb 0
#define EDM_STRIP_CLUSTER_CHANNELSPHI_mf 1.
#define EDM_STRIP_CLUSTER_CHANNELSPHI_s2c 0
#define EDM_STRIP_CLUSTER_CHANNELSPHI_decb 0
#define EDM_STRIP_CLUSTER_CHANNELSPHI_apfix 0

// EDM_PIXEL_CLUSTER word description
#define EDM_PIXEL_CLUSTER_bits         49
#define EDM_PIXEL_CLUSTER_LAST_bits    1
#define EDM_PIXEL_CLUSTER_LAST_msb     48
#define EDM_PIXEL_CLUSTER_LAST_lsb     48
#define EDM_PIXEL_CLUSTER_LAST_mf      1.
#define EDM_PIXEL_CLUSTER_LAST_s2c     0
#define EDM_PIXEL_CLUSTER_LAST_decb    0
#define EDM_PIXEL_CLUSTER_LAST_apfix   0

#define EDM_PIXEL_CLUSTER_COL_SIZE_bits 2
#define EDM_PIXEL_CLUSTER_COL_SIZE_msb 47
#define EDM_PIXEL_CLUSTER_COL_SIZE_lsb 46
#define EDM_PIXEL_CLUSTER_COL_SIZE_mf  1.
#define EDM_PIXEL_CLUSTER_COL_SIZE_s2c 0
#define EDM_PIXEL_CLUSTER_COL_SIZE_decb 0
#define EDM_PIXEL_CLUSTER_COL_SIZE_apfix 0

#define EDM_PIXEL_CLUSTER_COL_bits     13
#define EDM_PIXEL_CLUSTER_COL_msb      45
#define EDM_PIXEL_CLUSTER_COL_lsb      33
#define EDM_PIXEL_CLUSTER_COL_mf       1.
#define EDM_PIXEL_CLUSTER_COL_s2c      0
#define EDM_PIXEL_CLUSTER_COL_decb     0
#define EDM_PIXEL_CLUSTER_COL_apfix    0

#define EDM_PIXEL_CLUSTER_ROW_SIZE_bits 3
#define EDM_PIXEL_CLUSTER_ROW_SIZE_msb 32
#define EDM_PIXEL_CLUSTER_ROW_SIZE_lsb 30
#define EDM_PIXEL_CLUSTER_ROW_SIZE_mf  1.
#define EDM_PIXEL_CLUSTER_ROW_SIZE_s2c 0
#define EDM_PIXEL_CLUSTER_ROW_SIZE_decb 0
#define EDM_PIXEL_CLUSTER_ROW_SIZE_apfix 0

#define EDM_PIXEL_CLUSTER_ROW_bits     13
#define EDM_PIXEL_CLUSTER_ROW_msb      29
#define EDM_PIXEL_CLUSTER_ROW_lsb      17
#define EDM_PIXEL_CLUSTER_ROW_mf       1.
#define EDM_PIXEL_CLUSTER_ROW_s2c      0
#define EDM_PIXEL_CLUSTER_ROW_decb     0
#define EDM_PIXEL_CLUSTER_ROW_apfix    0

#define EDM_PIXEL_CLUSTER_NCLUSTERS_bits 1
#define EDM_PIXEL_CLUSTER_NCLUSTERS_msb 16
#define EDM_PIXEL_CLUSTER_NCLUSTERS_lsb 16
#define EDM_PIXEL_CLUSTER_NCLUSTERS_mf 1.
#define EDM_PIXEL_CLUSTER_NCLUSTERS_s2c 0
#define EDM_PIXEL_CLUSTER_NCLUSTERS_decb 0
#define EDM_PIXEL_CLUSTER_NCLUSTERS_apfix 0

#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_bits 1
#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_msb 15
#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_lsb 15
#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_mf 1.
#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_s2c 0
#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_decb 0
#define EDM_PIXEL_CLUSTER_CLUSTERIDHASH_apfix 0

#define EDM_PIXEL_CLUSTER_CLUSTERID_bits 1
#define EDM_PIXEL_CLUSTER_CLUSTERID_msb 14
#define EDM_PIXEL_CLUSTER_CLUSTERID_lsb 14
#define EDM_PIXEL_CLUSTER_CLUSTERID_mf 1.
#define EDM_PIXEL_CLUSTER_CLUSTERID_s2c 0
#define EDM_PIXEL_CLUSTER_CLUSTERID_decb 0
#define EDM_PIXEL_CLUSTER_CLUSTERID_apfix 0

#define EDM_PIXEL_CLUSTER_LOCALPOSITION_bits 1
#define EDM_PIXEL_CLUSTER_LOCALPOSITION_msb 13
#define EDM_PIXEL_CLUSTER_LOCALPOSITION_lsb 13
#define EDM_PIXEL_CLUSTER_LOCALPOSITION_mf 1.
#define EDM_PIXEL_CLUSTER_LOCALPOSITION_s2c 0
#define EDM_PIXEL_CLUSTER_LOCALPOSITION_decb 0
#define EDM_PIXEL_CLUSTER_LOCALPOSITION_apfix 0

#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_bits 1
#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_msb 12
#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_lsb 12
#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_mf 1.
#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_s2c 0
#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_decb 0
#define EDM_PIXEL_CLUSTER_LOCALCOVARIANCE_apfix 0

#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_bits 1
#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_msb 11
#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_lsb 11
#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_mf 1.
#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_s2c 0
#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_decb 0
#define EDM_PIXEL_CLUSTER_GLOBALPOSITION_apfix 0

#define EDM_PIXEL_CLUSTER_NPIXEL_bits  1
#define EDM_PIXEL_CLUSTER_NPIXEL_msb   10
#define EDM_PIXEL_CLUSTER_NPIXEL_lsb   10
#define EDM_PIXEL_CLUSTER_NPIXEL_mf    1.
#define EDM_PIXEL_CLUSTER_NPIXEL_s2c   0
#define EDM_PIXEL_CLUSTER_NPIXEL_decb  0
#define EDM_PIXEL_CLUSTER_NPIXEL_apfix 0

#define EDM_PIXEL_CLUSTER_PIXELID_bits 1
#define EDM_PIXEL_CLUSTER_PIXELID_msb  9
#define EDM_PIXEL_CLUSTER_PIXELID_lsb  9
#define EDM_PIXEL_CLUSTER_PIXELID_mf   1.
#define EDM_PIXEL_CLUSTER_PIXELID_s2c  0
#define EDM_PIXEL_CLUSTER_PIXELID_decb 0
#define EDM_PIXEL_CLUSTER_PIXELID_apfix 0

#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_bits 1
#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_msb 8
#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_lsb 8
#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_mf 1.
#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_s2c 0
#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_decb 0
#define EDM_PIXEL_CLUSTER_CHANNELSINPHI_apfix 0

#define EDM_PIXEL_CLUSTER_CHANNELSINETA_bits 1
#define EDM_PIXEL_CLUSTER_CHANNELSINETA_msb 7
#define EDM_PIXEL_CLUSTER_CHANNELSINETA_lsb 7
#define EDM_PIXEL_CLUSTER_CHANNELSINETA_mf 1.
#define EDM_PIXEL_CLUSTER_CHANNELSINETA_s2c 0
#define EDM_PIXEL_CLUSTER_CHANNELSINETA_decb 0
#define EDM_PIXEL_CLUSTER_CHANNELSINETA_apfix 0

#define EDM_PIXEL_CLUSTER_WIDTHINETA_bits 1
#define EDM_PIXEL_CLUSTER_WIDTHINETA_msb 6
#define EDM_PIXEL_CLUSTER_WIDTHINETA_lsb 6
#define EDM_PIXEL_CLUSTER_WIDTHINETA_mf 1.
#define EDM_PIXEL_CLUSTER_WIDTHINETA_s2c 0
#define EDM_PIXEL_CLUSTER_WIDTHINETA_decb 0
#define EDM_PIXEL_CLUSTER_WIDTHINETA_apfix 0

#define EDM_PIXEL_CLUSTER_OMEGAX_bits  1
#define EDM_PIXEL_CLUSTER_OMEGAX_msb   5
#define EDM_PIXEL_CLUSTER_OMEGAX_lsb   5
#define EDM_PIXEL_CLUSTER_OMEGAX_mf    1.
#define EDM_PIXEL_CLUSTER_OMEGAX_s2c   0
#define EDM_PIXEL_CLUSTER_OMEGAX_decb  0
#define EDM_PIXEL_CLUSTER_OMEGAX_apfix 0

#define EDM_PIXEL_CLUSTER_OMEGAY_bits  1
#define EDM_PIXEL_CLUSTER_OMEGAY_msb   4
#define EDM_PIXEL_CLUSTER_OMEGAY_lsb   4
#define EDM_PIXEL_CLUSTER_OMEGAY_mf    1.
#define EDM_PIXEL_CLUSTER_OMEGAY_s2c   0
#define EDM_PIXEL_CLUSTER_OMEGAY_decb  0
#define EDM_PIXEL_CLUSTER_OMEGAY_apfix 0

#define EDM_PIXEL_CLUSTER_NTOT_bits    1
#define EDM_PIXEL_CLUSTER_NTOT_msb     3
#define EDM_PIXEL_CLUSTER_NTOT_lsb     3
#define EDM_PIXEL_CLUSTER_NTOT_mf      1.
#define EDM_PIXEL_CLUSTER_NTOT_s2c     0
#define EDM_PIXEL_CLUSTER_NTOT_decb    0
#define EDM_PIXEL_CLUSTER_NTOT_apfix   0

#define EDM_PIXEL_CLUSTER_TOTVALUE_bits 1
#define EDM_PIXEL_CLUSTER_TOTVALUE_msb 2
#define EDM_PIXEL_CLUSTER_TOTVALUE_lsb 2
#define EDM_PIXEL_CLUSTER_TOTVALUE_mf  1.
#define EDM_PIXEL_CLUSTER_TOTVALUE_s2c 0
#define EDM_PIXEL_CLUSTER_TOTVALUE_decb 0
#define EDM_PIXEL_CLUSTER_TOTVALUE_apfix 0

#define EDM_PIXEL_CLUSTER_NCHARGE_bits 1
#define EDM_PIXEL_CLUSTER_NCHARGE_msb  1
#define EDM_PIXEL_CLUSTER_NCHARGE_lsb  1
#define EDM_PIXEL_CLUSTER_NCHARGE_mf   1.
#define EDM_PIXEL_CLUSTER_NCHARGE_s2c  0
#define EDM_PIXEL_CLUSTER_NCHARGE_decb 0
#define EDM_PIXEL_CLUSTER_NCHARGE_apfix 0

#define EDM_PIXEL_CLUSTER_CHARGEVALUES_bits 1
#define EDM_PIXEL_CLUSTER_CHARGEVALUES_msb 0
#define EDM_PIXEL_CLUSTER_CHARGEVALUES_lsb 0
#define EDM_PIXEL_CLUSTER_CHARGEVALUES_mf 1.
#define EDM_PIXEL_CLUSTER_CHARGEVALUES_s2c 0
#define EDM_PIXEL_CLUSTER_CHARGEVALUES_decb 0
#define EDM_PIXEL_CLUSTER_CHARGEVALUES_apfix 0

// PIXEL_EF_RDO word description
#define PIXEL_EF_RDO_bits              64
#define PIXEL_EF_RDO_LAST_bits         1
#define PIXEL_EF_RDO_LAST_msb          63
#define PIXEL_EF_RDO_LAST_lsb          63
#define PIXEL_EF_RDO_LAST_mf           1.
#define PIXEL_EF_RDO_LAST_s2c          0
#define PIXEL_EF_RDO_LAST_decb         0
#define PIXEL_EF_RDO_LAST_apfix        0

#define PIXEL_EF_RDO_ROW_bits          10
#define PIXEL_EF_RDO_ROW_msb           62
#define PIXEL_EF_RDO_ROW_lsb           53
#define PIXEL_EF_RDO_ROW_mf            1.
#define PIXEL_EF_RDO_ROW_s2c           0
#define PIXEL_EF_RDO_ROW_decb          0
#define PIXEL_EF_RDO_ROW_apfix         0

#define PIXEL_EF_RDO_COL_bits          10
#define PIXEL_EF_RDO_COL_msb           52
#define PIXEL_EF_RDO_COL_lsb           43
#define PIXEL_EF_RDO_COL_mf            1.
#define PIXEL_EF_RDO_COL_s2c           0
#define PIXEL_EF_RDO_COL_decb          0
#define PIXEL_EF_RDO_COL_apfix         0

#define PIXEL_EF_RDO_TOT_bits          4
#define PIXEL_EF_RDO_TOT_msb           42
#define PIXEL_EF_RDO_TOT_lsb           39
#define PIXEL_EF_RDO_TOT_mf            1.
#define PIXEL_EF_RDO_TOT_s2c           0
#define PIXEL_EF_RDO_TOT_decb          0
#define PIXEL_EF_RDO_TOT_apfix         0

#define PIXEL_EF_RDO_LVL1_bits         1
#define PIXEL_EF_RDO_LVL1_msb          38
#define PIXEL_EF_RDO_LVL1_lsb          38
#define PIXEL_EF_RDO_LVL1_mf           1.
#define PIXEL_EF_RDO_LVL1_s2c          0
#define PIXEL_EF_RDO_LVL1_decb         0
#define PIXEL_EF_RDO_LVL1_apfix        0

#define PIXEL_EF_RDO_ID_bits           13
#define PIXEL_EF_RDO_ID_msb            37
#define PIXEL_EF_RDO_ID_lsb            25
#define PIXEL_EF_RDO_ID_mf             1.
#define PIXEL_EF_RDO_ID_s2c            0
#define PIXEL_EF_RDO_ID_decb           0
#define PIXEL_EF_RDO_ID_apfix          0

#define PIXEL_EF_RDO_SPARE_bits        25
#define PIXEL_EF_RDO_SPARE_msb         24
#define PIXEL_EF_RDO_SPARE_lsb         0
#define PIXEL_EF_RDO_SPARE_mf          1.
#define PIXEL_EF_RDO_SPARE_s2c         0
#define PIXEL_EF_RDO_SPARE_decb        0
#define PIXEL_EF_RDO_SPARE_apfix       0

// STRIP_EF_RDO word description
#define STRIP_EF_RDO_bits              32
#define STRIP_EF_RDO_LAST_bits         1
#define STRIP_EF_RDO_LAST_msb          31
#define STRIP_EF_RDO_LAST_lsb          31
#define STRIP_EF_RDO_LAST_mf           1.
#define STRIP_EF_RDO_LAST_s2c          0
#define STRIP_EF_RDO_LAST_decb         0
#define STRIP_EF_RDO_LAST_apfix        0

#define STRIP_EF_RDO_CHIPID_bits       4
#define STRIP_EF_RDO_CHIPID_msb        30
#define STRIP_EF_RDO_CHIPID_lsb        27
#define STRIP_EF_RDO_CHIPID_mf         1.
#define STRIP_EF_RDO_CHIPID_s2c        0
#define STRIP_EF_RDO_CHIPID_decb       0
#define STRIP_EF_RDO_CHIPID_apfix      0

#define STRIP_EF_RDO_STRIP_NUM_bits    8
#define STRIP_EF_RDO_STRIP_NUM_msb     26
#define STRIP_EF_RDO_STRIP_NUM_lsb     19
#define STRIP_EF_RDO_STRIP_NUM_mf      1.
#define STRIP_EF_RDO_STRIP_NUM_s2c     0
#define STRIP_EF_RDO_STRIP_NUM_decb    0
#define STRIP_EF_RDO_STRIP_NUM_apfix   0

#define STRIP_EF_RDO_CLUSTER_MAP_bits  3
#define STRIP_EF_RDO_CLUSTER_MAP_msb   18
#define STRIP_EF_RDO_CLUSTER_MAP_lsb   16
#define STRIP_EF_RDO_CLUSTER_MAP_mf    1.
#define STRIP_EF_RDO_CLUSTER_MAP_s2c   0
#define STRIP_EF_RDO_CLUSTER_MAP_decb  0
#define STRIP_EF_RDO_CLUSTER_MAP_apfix 0

#define STRIP_EF_RDO_ID_bits           13
#define STRIP_EF_RDO_ID_msb            15
#define STRIP_EF_RDO_ID_lsb            3
#define STRIP_EF_RDO_ID_mf             1.
#define STRIP_EF_RDO_ID_s2c            0
#define STRIP_EF_RDO_ID_decb           0
#define STRIP_EF_RDO_ID_apfix          0

#define STRIP_EF_RDO_SPARE_bits        3
#define STRIP_EF_RDO_SPARE_msb         2
#define STRIP_EF_RDO_SPARE_lsb         0
#define STRIP_EF_RDO_SPARE_mf          1.
#define STRIP_EF_RDO_SPARE_s2c         0
#define STRIP_EF_RDO_SPARE_decb        0
#define STRIP_EF_RDO_SPARE_apfix       0

#define PIXEL_RAW_BITS 32

