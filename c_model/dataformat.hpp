#ifndef _DATAFORMAT_HPP_
#define _DATAFORMAT_HPP_

#include <ap_int.h>
#include <ap_fixed.h>

#include "bitfield_definitions.h"

#define FLAG_bits       EVT_FTR_W1_FLAG_bits
#define FLAG_lsb        EVT_FTR_W1_FLAG_lsb
#define QWORD_bits      (WORD_LENGTH * 2U)

typedef ap_uint<QWORD_bits> apui_qw;

#define EVT_HDR_FLAG    0xab

#define MAX_MODULES 27052

// Bits: 6 * (64 + 8 (meta))
typedef struct event_header {
    // QWORD #0
    ap_uint<EVT_HDR_W1_FLAG_bits> flag = EVT_HDR_FLAG;
    ap_uint<EVT_HDR_W1_L0ID_bits> l0id;
    ap_uint<EVT_HDR_W1_BCID_bits> bcid;
    ap_uint<EVT_HDR_W1_SPARE_bits> qw0spare;
    // QWORD #1
    // ap_uint<EVT_HDR_W2_SPARE_bits> qw1spare;
    ap_uint<EVT_HDR_W2_RUNNUMBER_bits> runnumber;
    ap_uint<EVT_HDR_W2_TIME_bits> time; 
    // QWORD #2
    ap_uint<EVT_HDR_W3_STATUS_bits> status;
    ap_uint<EVT_HDR_W3_CRC_bits> crc;

} event_header;

#define EVT_FTR_FLAG    0xcd

// Bits: 3 * (64 + 8 (meta))
typedef struct event_footer {
    // QWORD #0
    ap_uint<EVT_FTR_W1_FLAG_bits> flag = EVT_FTR_FLAG;
    ap_uint<EVT_FTR_W1_SPARE_bits> spare;
    ap_uint<EVT_FTR_W1_HDR_CRC_bits> hdr_crc;
    // QWORD #1
    ap_uint<EVT_FTR_W2_ERROR_FLAGS_bits> error_flags;
    // QWORD #2
    ap_uint<EVT_FTR_W3_WORD_COUNT_bits> word_count;
    ap_uint<EVT_FTR_W3_CRC_bits> crc;
} event_footer;

#define MODULE_TYPE_PIXEL 0x0
#define MODULE_TYPE_STRIP 0x1

#define M_HDR_FLAG 0x55

// Bits: 64 + 32 + 2 * 8 (meta)
typedef struct module_header {
    // QWORD #0
    ap_uint<M_HDR_W1_FLAG_bits> flag = M_HDR_FLAG;
    ap_uint<M_HDR_W1_DET_bits> det;
    ap_uint<M_HDR_W1_MODID_bits> modid;
    ap_uint<M_HDR_W1_MODULE_TYPE_bits> modtype;
    ap_uint<M_HDR_W1_PHI_INDEX_bits > phi_index;
    ap_uint<M_HDR_W1_ETA_INDEX_bits > eta_index;
    ap_uint<M_HDR_W1_LAYER_bits > layer;
    ap_uint<M_HDR_W1_SPARE_bits> mw0spare;

} module_header;


typedef ap_ufixed<GHITZ_W1_RAD_bits, GHITZ_W1_RAD_bits - GHITZ_W1_RAD_apfix> apuf_rad;
typedef ap_fixed<GHITZ_W1_PHI_bits, GHITZ_W1_PHI_bits - GHITZ_W1_PHI_apfix> apf_gphi;
typedef ap_fixed<GHITZ_W1_Z_bits, GHITZ_W1_Z_bits - GHITZ_W1_Z_apfix> apf_z;

typedef ap_ufixed<GHITZ_W1_RAD_bits + 2, GHITZ_W1_RAD_bits - GHITZ_W1_RAD_apfix> apuf_rad_temp;
//typedef ap_fixed<GHITZ_PHI_bits + 3, GHITZ_PHI_bits + 1 - GHITZ_PHI_apfix> apf_gphi_temp;
typedef ap_fixed<GHITZ_W1_PHI_bits + 10, GHITZ_W1_PHI_bits + 7 - GHITZ_W1_PHI_apfix> apf_gphi_temp;
typedef ap_fixed<GHITZ_W1_Z_bits + 2, GHITZ_W1_Z_bits - GHITZ_W1_LAST_apfix> apf_z_temp;

// // Structs for hits and space-points used internally
// typedef struct hit_coords {
//     apuf_rad r;
//     apf_gphi gPhi;
//     apf_z z;
// } hit_coords;


// typedef struct sphit {
//     ap_uint<1> isPixel;
//     ap_uint<4> layer;
//     apuf_rad r;
//     apf_gphi gPhi;
//     apf_z z;
// } sphit;

// typedef struct ghitz {
//     //ap_uint<GHITZ_LYR_bits> layer;
//     ap_uint<GHITZ_RAD_bits> r;
//     ap_uint<GHITZ_PHI_bits> gPhi;
//     ap_uint<GHITZ_Z_bits> z;
// } ghitz;

typedef struct pixel_cluster{
    ap_uint<10> col; //local phi coordinate
    ap_uint<10> row; //local eta coordinate

    ap_fixed<10, 10> x; // global coordinates
    ap_fixed<10, 10> y;
    ap_fixed<10, 10> z;

    ap_fixed<10, 10> r;
    ap_fixed<10, 10> phi; // Dataformat requires cylindrical coordinates

    ap_uint<4> layer;

} pixel_cluster;

typedef struct strip_cluster{
    int row;
    int nstrips;
    int strip_index;

    ap_fixed<10, 10> x; // global coordinates
    ap_fixed<10, 10> y;
    ap_fixed<10, 10> z;

    ap_fixed<10, 10> r;
    ap_fixed<10, 10> phi; // Dataformat requires cylindrical coordinates

    ap_uint<4> layer;
} strip_cluster;




typedef struct module_ghitz {
    module_header mhead;
    ap_uint<1> isPixel; // obtain from module header DET, 0 if pixel, 1 if strip
    int layer;
    int modeta;
    int modphi;
    int num_clusters;
    ap_fixed<1,35> rotation_matrix[9]; //Number of bits and parameters is likely to change
    ap_fixed<10,22> translation_vector[3]; 

} module_ghitz;

typedef struct hit {
    ap_uint<2> zone;
    ap_uint<4> layer;
    ap_uint<2> stave;
    ap_uint<1> side;
    ap_int<6> modeta;
    ap_int<4> modphi;
    apuf_rad r;
    apf_gphi gPhi;
    apf_z z;
} hit;

typedef struct event {
    event_header ehead;
    event_footer efoot;
} event;

#define PIXMODS_MAXCOUNT 256
#define PIXMODS_MAXCOUNT_SIZE 8
#define STRMODS_MAXCOUNT 256
#define STRMODS_MAXCOUNT_SIZE 8
#define MODS_MAXCOUNT_MAXSIZE ((PIXMODS_MAXCOUNT_SIZE > STRMODS_MAXCOUNT_SIZE ? PIXMODS_MAXCOUNT_SIZE : STRMODS_MAXCOUNT_SIZE) + 1)
#define MODS_MAXCOUNT (PIXMODS_MAXCOUNT > STRMODS_MAXCOUNT ? PIXMODS_MAXCOUNT : STRMODS_MAXCOUNT)
// Bits: 6 * (64 + 8 (meta))
// Bits: 8336 * 256
// Bits: 3216 * 256
// Bits: 3 * (64 + 8 (meta))
// Sum: 2957960
#define EVENT_MINSIZE ((792 + 40)/ QWORD_bits) // size if there is only one strip hit (+40 to get number divisible by 64)
#define EVENT_MAXSIZE ((2957960 + 56) / QWORD_bits) // add 56 to get number divisible by 64

#endif // _DATAFORMAT_HPP_
