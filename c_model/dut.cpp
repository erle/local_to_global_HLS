#include <ap_int.h>
#include <ap_fixed.h>
#include <hls_math.h>
#include <hls_stream.h>

#include "dataformat.hpp"
#include "dut.hpp"

// Only include one lookup table
#include "lut.hpp" 
// #include "pixels_lookup_table.h"
// #include "athena_dump_table.h"

#include "hls_print.h"

#define XXX(MSG, ...) hls::print(MSG, __VA_ARGS__)
#define DEBUGOUTPUT 0



void inline decode_event_header(hls::stream<apui_qw> &inStream, unsigned int *i)
{
    apui_qw buf; // A variable to temporarily store each quad word read from the stream.

    // First quad word
    read_qword(&buf, inStream, i);
    ev.ehead.flag = (buf & SELECTBITS(EVT_HDR_W1_FLAG_bits, EVT_HDR_W1_FLAG_lsb)) >> EVT_HDR_W1_FLAG_lsb;
    ev.ehead.l0id = (buf & SELECTBITS(EVT_HDR_W1_L0ID_bits, EVT_HDR_W1_L0ID_lsb)) >> EVT_HDR_W1_L0ID_lsb;
    ev.ehead.bcid = (buf & SELECTBITS(EVT_HDR_W1_BCID_bits, EVT_HDR_W1_BCID_lsb)) >> EVT_HDR_W1_BCID_lsb;
    ev.ehead.qw0spare = (buf & SELECTBITS(EVT_HDR_W1_SPARE_bits, EVT_HDR_W1_SPARE_lsb)); // Note that spare lsb equals 0, so no shift needed

    // Second quad word
    read_qword(&buf, inStream, i);
    // ev.ehead.bcid = (buf & SELECTBITS(EVT_HDR_W2_BCID_bits, EVT_HDR_W2_BCID_lsb)) >> EVT_HDR_W2_BCID_lsb;
    // ev.ehead.qw1spare = (buf & SELECTBITS(EVT_HDR_W2_SPARE_bits, EVT_HDR_W2_SPARE_lsb)) >> EVT_HDR_W2_SPARE_lsb;
    ev.ehead.runnumber = (buf & SELECTBITS(EVT_HDR_W2_RUNNUMBER_bits, EVT_HDR_W2_RUNNUMBER_lsb)) >> EVT_HDR_W2_RUNNUMBER_lsb;  // No shift needed
    ev.ehead.time = buf & SELECTBITS(EVT_HDR_W2_TIME_bits, EVT_HDR_W2_TIME_lsb); // No shift needed

    // Third quad word
    read_qword(&buf, inStream, i);
    ev.ehead.status = (buf & SELECTBITS(EVT_HDR_W3_STATUS_bits, EVT_HDR_W3_STATUS_lsb)) >> EVT_HDR_W3_STATUS_lsb;
    ev.ehead.crc = buf & SELECTBITS(EVT_HDR_W3_CRC_bits, EVT_HDR_W3_CRC_lsb);  // No shift needed
}


void inline decode_event_footer(hls::stream<apui_qw> &inStream, unsigned int *i, apui_qw buf)
{
    // Decode fields from the first quad word (already read into buf)
    ev.efoot.flag = (buf & SELECTBITS(EVT_FTR_W1_FLAG_bits, EVT_FTR_W1_FLAG_lsb)) >> EVT_FTR_W1_FLAG_lsb;
    ev.efoot.spare = (buf & SELECTBITS(EVT_FTR_W1_SPARE_bits, EVT_FTR_W1_SPARE_lsb)) >> EVT_FTR_W1_SPARE_lsb;
    ev.efoot.hdr_crc = buf & SELECTBITS(EVT_FTR_W1_HDR_CRC_bits, EVT_FTR_W1_HDR_CRC_lsb);

    // Read and decode the second quad word
    read_qword(&ev.efoot.error_flags, inStream, i);

    // Read and decode the third quad word
    read_qword(&buf, inStream, i);
    ev.efoot.word_count = (buf & SELECTBITS(EVT_FTR_W3_WORD_COUNT_bits, EVT_FTR_W3_WORD_COUNT_lsb)) >> EVT_FTR_W3_WORD_COUNT_lsb;
    ev.efoot.crc = buf & SELECTBITS(EVT_FTR_W3_CRC_bits, EVT_FTR_W3_CRC_lsb);
}

ap_uint<FLAG_bits> decode_module(hls::stream<apui_qw> &inStream, unsigned int *i, apui_qw buf)
{
    module_ghitz m;

    // Decode fields from the single quad word (already read into buf)
    m.mhead.flag = (buf & SELECTBITS(M_HDR_W1_FLAG_bits, M_HDR_W1_FLAG_lsb)) >> M_HDR_W1_FLAG_lsb;
    m.mhead.det = (buf & SELECTBITS(M_HDR_W1_DET_bits, M_HDR_W1_DET_lsb)) >> M_HDR_W1_DET_lsb;
    m.mhead.modid = (buf & SELECTBITS(M_HDR_W1_MODID_bits, M_HDR_W1_MODID_lsb)) >> M_HDR_W1_MODID_lsb;
    m.mhead.modtype = (buf & SELECTBITS(M_HDR_W1_MODULE_TYPE_bits, M_HDR_W1_MODULE_TYPE_lsb)) >> M_HDR_W1_MODULE_TYPE_lsb;
    m.mhead.phi_index = (buf & SELECTBITS(M_HDR_W1_PHI_INDEX_bits, M_HDR_W1_PHI_INDEX_lsb)) >> M_HDR_W1_PHI_INDEX_lsb;
    m.mhead.eta_index = (buf & SELECTBITS(M_HDR_W1_ETA_INDEX_bits, M_HDR_W1_ETA_INDEX_lsb)) >> M_HDR_W1_ETA_INDEX_lsb;
    m.mhead.layer = (buf & SELECTBITS(M_HDR_W1_LAYER_bits, M_HDR_W1_LAYER_lsb)) >> M_HDR_W1_LAYER_lsb;
    m.mhead.mw0spare = buf & SELECTBITS(M_HDR_W1_SPARE_bits, M_HDR_W1_SPARE_lsb); // No shift needed


    // modules[] saves the module header and number of clusters which is need for encoding the event
    modules[module_count].mhead = m.mhead; // Saving module headers
    modules[module_count].num_clusters = 0; // Initialize number of clusters

    // modid's from Julian's dump file probably doesn't match mod id from Jason's dataformat
    // There are 27052 entries in lookup_table from athena_dump_table.hpp

    map_modid:
            // Assume lookup table is from lut.hpp
        for (int j = 0; j < 27052; ++j) {
            // ++module_count;
        DO_PRAGMA(HLS loop_tripcount min = 1 max = 27052)
            // if (m.mhead.modid == modid_map[j][1]) {
            //     m.detector = modid_map[j][1];
            //     m.modeta = modid_map[j][2];
            //     m.modphi = modid_map[j][3];
            //     m.layer = modid_map[j][4];
            //     m.isPixel = modid_map[j][5];

            if (m.mhead.modid == modid_map[j][0]){
                // m.detector = modid_map[j][1] // don't have this information in lut.hpp
                m.isPixel == m.mhead.det; // 0 if pixel, 1 if strip

                for (int i = 0; i < 6; ++i) {
                    m.rotation_matrix[i] = modid_map[j][i + 1]; // Last 3 parameters of rotation_matrix can be 0
                }

                for (int i=0; i< 3; i++){
                    m.translation_vector[i] = modid_map[j][i+9];
                }
                }
    #if DEBUGOUTPUT
                XXX("Module Zone: %d\n", (unsigned int)m.zone);
                XXX("Module modeta: %d\n", (unsigned int)m.modeta);
                XXX("Module modphi: %d\n", (unsigned int)m.modphi);
                XXX("Module layer: %d\n", (unsigned int)m.layer);
    #endif
            }


decode_hits:
    while (hits_pix_count + hits_strip_count < 4096) { // 4096 maximum number of hits per module header
    DO_PRAGMA(HLS loop_tripcount min = 1 max = 4096)
    DO_PRAGMA(HLS pipeline II=2)
        read_qword(&buf, inStream, i);
        ap_uint<FLAG_bits> flag = (buf & SELECTBITS(FLAG_bits, FLAG_lsb)) >> FLAG_lsb;

        // read_qword reads a 64-bit value from the stream into buf. The flag is extracted. If the flag is a module header or event footer, then it returns the flag. 
        // If the flag is neither the module header or event footer, we assume that it is a pixel cluster output. 


        // Goes back to decode_event and if there is another module header, then ret==M_HDR_FLAG. And decode_module is called again. 
        if (flag == ((ap_uint<FLAG_bits>) M_HDR_FLAG) || flag == ((ap_uint<FLAG_bits>) EVT_FTR_FLAG)) {
            return flag;
        } else {
            if (m.isPixel == 0) {
                pixel_hits[hits_pix_count].col = ((ap_uint<PIXEL_CLUSTER_COL_bits>)((buf & SELECTBITS(PIXEL_CLUSTER_COL_bits, PIXEL_CLUSTER_COL_lsb)) >> PIXEL_CLUSTER_COL_lsb));
                pixel_hits[hits_pix_count].row = ((ap_uint<PIXEL_CLUSTER_ROW_bits>)((buf & SELECTBITS(PIXEL_CLUSTER_ROW_bits, PIXEL_CLUSTER_ROW_lsb)) >> PIXEL_CLUSTER_ROW_lsb));

                // Calculate centroid
                pixel_hits[hits_pix_count].col = pixel_hits[hits_pix_count].col + (1/2)*((ap_uint<PIXEL_CLUSTER_COL_SIZE_bits>)((buf & SELECTBITS(PIXEL_CLUSTER_COL_SIZE_bits, PIXEL_CLUSTER_COL_SIZE_lsb)) >> PIXEL_CLUSTER_COL_SIZE_lsb));
                pixel_hits[hits_pix_count].row = pixel_hits[hits_pix_count].row + (1/2)*((ap_uint<PIXEL_CLUSTER_ROW_SIZE_bits>)((buf & SELECTBITS(PIXEL_CLUSTER_ROW_SIZE_bits, PIXEL_CLUSTER_ROW_SIZE_lsb)) >> PIXEL_CLUSTER_ROW_SIZE_lsb));

                
                pixel_hits[hits_pix_count].x =  m.translation_vector[0] + (m.rotation_matrix[0]*pixel_hits[hits_pix_count].col + m.rotation_matrix[3]*pixel_hits[hits_pix_count].row); //global hits
                pixel_hits[hits_pix_count].y = m.translation_vector[1] + (m.rotation_matrix[1]*pixel_hits[hits_pix_count].col + m.rotation_matrix[4]*pixel_hits[hits_pix_count].row);
                pixel_hits[hits_pix_count].z = m.translation_vector[2] + (m.rotation_matrix[2]*pixel_hits[hits_pix_count].col + m.rotation_matrix[5]*pixel_hits[hits_pix_count].row);

                pixel_hits[hits_pix_count].r = hls::sqrt(pixel_hits[hits_pix_count].x + pixel_hits[hits_pix_count].y );
                pixel_hits[hits_pix_count].phi = hls::atan2(pixel_hits[hits_pix_count].y, pixel_hits[hits_pix_count].x); //Note that atan2 correctly handles the sign of the argument

                pixel_hits[hits_pix_count].layer = m.layer;

// #if DEBUGOUTPUT
//                 XXX("Decoding pixel hit number %d\n", (unsigned int)hits_sp_count);
//                 XXX(" Logical layer: %d\n", (unsigned int)hits_sp[hits_sp_count].layer);
//                 XXX(" R: %f ", (double)hits_sp[hits_sp_count].r);
//                 XXX(" = %d\n", (unsigned int)(hits_sp[hits_sp_count].r * (ap_ufixed<GHITZ_RAD_apfix + 1,GHITZ_RAD_apfix + 1>)GHITZ_RAD_mf));
//                 XXX(" gPhi: %f ", (double)hits_sp[hits_sp_count].gPhi);
//                 XXX(" = %d\n", (unsigned int)(hits_sp[hits_sp_count].gPhi * (ap_ufixed<GHITZ_PHI_apfix + 1,GHITZ_PHI_apfix + 1>)GHITZ_PHI_mf));
//                 XXX(" Z: %f ", (double)hits_sp[hits_sp_count].z);
//                 XXX(" = %d\n\n", (int)(hits_sp[hits_sp_count].z * (ap_ufixed<GHITZ_Z_apfix + 1,GHITZ_Z_apfix + 1>)GHITZ_Z_mf));
// #endif
                ++hits_pix_count;
                ++modules[module_count].num_clusters;
            } 
            else { // for strips to be done later
                strip_hits[hits_strip_count].row = ((ap_uint<STRIP_CLUSTER_ROW_bits>)((buf & SELECTBITS(STRIP_CLUSTER_ROW_bits, STRIP_CLUSTER_ROW_lsb)) >> STRIP_CLUSTER_ROW_lsb));
                strip_hits[hits_strip_count].nstrips = ((ap_uint<STRIP_CLUSTER_NSTRIPS_bits>)((buf & SELECTBITS(STRIP_CLUSTER_NSTRIPS_bits, STRIP_CLUSTER_NSTRIPS_lsb)) >> STRIP_CLUSTER_NSTRIPS_lsb));
                strip_hits[hits_strip_count].strip_index = ((ap_uint<STRIP_CLUSTER_STRIP_INDEX_bits>)((buf & SELECTBITS(STRIP_CLUSTER_STRIP_INDEX_bits, STRIP_CLUSTER_STRIP_INDEX_lsb)) >> STRIP_CLUSTER_STRIP_INDEX_lsb));
                // local to global calculation done here



//                 hits[hits_count].zone = m.zone;
//                 hits[hits_count].layer = ((ap_uint<GHITZ_LYR_bits>)((buf & SELECTBITS(GHITZ_LYR_bits, GHITZ_LYR_lsb)) >> GHITZ_LYR_lsb));
//                 hits[hits_count].stave = m.layer / 2;
//                 hits[hits_count].side = m.layer % 2;
//                 hits[hits_count].modeta = m.modeta;
//                 hits[hits_count].modphi = m.modphi;
//                 hits[hits_count].r = ((ap_uint<GHITZ_RAD_bits>)((buf & SELECTBITS(GHITZ_RAD_bits, GHITZ_RAD_lsb)) >> GHITZ_RAD_lsb)) / GHITZ_RAD_mf; // radius in milli-metres
//                 hits[hits_count].gPhi = ((ap_uint<GHITZ_PHI_bits>)((buf & SELECTBITS(GHITZ_PHI_bits, GHITZ_PHI_lsb)) >> GHITZ_PHI_lsb)) / GHITZ_PHI_mf;
//                 hits[hits_count].z = ((ap_int<GHITZ_Z_bits>)((buf & SELECTBITS(GHITZ_Z_bits, GHITZ_Z_lsb)) >> GHITZ_Z_lsb)) / GHITZ_Z_mf;
// #if DEBUGOUTPUT
//                 XXX("Decoding strip hit number %d\n", (unsigned int)hits_count);
//                 XXX(" Logical layer %d\n", (unsigned int)hits[hits_count].layer);
//                 XXX(" Stave %d\n", (unsigned int)hits[hits_count].stave);
//                 XXX(" Side: %d\n", (unsigned int)hits[hits_count].side);
//                 XXX(" R: %f ", (double)hits[hits_count].r);
//                 XXX(" = %d\n", (unsigned int)(hits[hits_count].r * (ap_ufixed<GHITZ_RAD_apfix + 1,GHITZ_RAD_apfix + 1>)GHITZ_RAD_mf));
//                 XXX(" gPhi: %f ", (double)hits[hits_count].gPhi);
//                 XXX(" = %d\n", (unsigned int)(hits[hits_count].gPhi * (ap_ufixed<GHITZ_PHI_apfix + 1,GHITZ_PHI_apfix + 1>)GHITZ_PHI_mf));
//                 XXX(" Z: %f ", (double)hits[hits_count].z);
//                 XXX(" = %d\n\n", (int)(hits[hits_count].z * (ap_ufixed<GHITZ_Z_apfix + 1,GHITZ_Z_apfix + 1>)GHITZ_Z_mf));
// #endif
                ++hits_strip_count;
                ++modules[module_count].num_clusters;
            }
        }
    }
    // return m.mhead.flag;
    return 0xff; // After while loop ends, decode_module for loop will also end, since ret won't equal module header flag anymore
}


void decode_event(hls::stream<apui_qw> &inStream, unsigned int vSize)
{
    unsigned int i = 0;
    apui_qw buf;
    ap_uint<FLAG_bits> ret;


    decode_event_header(inStream, &i);

    read_qword(&buf, inStream, &i);
    if (((buf & SELECTBITS(FLAG_bits, FLAG_lsb)) >> FLAG_lsb) == M_HDR_FLAG) {
        // do-while would be better but this makes it possible to estimate run-time
        ret = decode_module(inStream, &i, buf);
        for (; ret == M_HDR_FLAG && i < vSize ;) {
        DO_PRAGMA(HLS loop_tripcount min = EVENT_MINSIZE max = EVENT_MAXSIZE)
        // Loop will proceed if ret == M_HDR_FLAG
        // Would be nice if module header had a LAST field
            // if (module_count < MAX_MODULES){
                // ret = decode_module(inStream, &i, buf); 
                // modules[module_count] = m.mhead; // Saving module headers
                // ++module_count;
            ret = decode_module(inStream, &i, buf); 
            ++module_count;
        }
    }
    decode_event_footer(inStream, &i, buf);

}

void calculate_crc()
{
    // TODO
}

void inline encode_event_header(hls::stream<apui_qw>& outStream)
{
    apui_qw buf = 0;

    // First quad word
    buf = (apui_qw)ev.ehead.flag << EVT_HDR_W1_FLAG_lsb;
    buf |= (apui_qw)ev.ehead.l0id << EVT_HDR_W1_L0ID_lsb;
    buf |= (apui_qw)ev.ehead.bcid << EVT_HDR_W1_BCID_lsb;
    buf |= (apui_qw)ev.ehead.qw0spare; // lsb for last field in word is 0, so not bitshifting needed
    outStream << buf;

    // Second quad word
    buf = (apui_qw)ev.ehead.runnumber << EVT_HDR_W2_RUNNUMBER_lsb;
    buf |= (apui_qw)ev.ehead.time; // No shifiting
    outStream << buf;

    // Third quad word
    buf = (apui_qw)ev.ehead.status << EVT_HDR_W3_STATUS_lsb;
    buf |= (apui_qw)ev.ehead.crc; // No shifiting
    outStream << buf;
}

void inline encode_module_header(hls::stream<apui_qw>& outStream, int mod_count){
    apui_qw buf = 0;

    // Module header only has one quad word
    buf = (apui_qw) modules[mod_count].mhead.flag << M_HDR_W1_FLAG_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.det << M_HDR_W1_DET_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.modid << M_HDR_W1_MODID_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.modtype << M_HDR_W1_MODULE_TYPE_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.phi_index << M_HDR_W1_PHI_INDEX_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.eta_index << M_HDR_W1_ETA_INDEX_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.layer << M_HDR_W1_LAYER_lsb;
    buf |= (apui_qw) modules[mod_count].mhead.mw0spare << M_HDR_W1_SPARE_lsb;

    outStream << buf;
}

void inline encode_ghitz(hls::stream<apui_qw>& outStream, int mod_count)
{

    #if DEBUGOUTPUT
        XXX("Number of strip hits: %d\n", (unsigned int)(hits_strip_count));
        XXX("Number of pixel hits: %d\n", (unsigned int)hits_pix_count);
    #endif    

    // Check the detector type and call the appropriate encoding function
    if (modules[mod_count].mhead.det == 0) {
        encode_ghitz_pix(outStream, mod_count);
    } else {
        encode_ghitz_strip(outStream,mod_count);
    }
}
void inline encode_ghitz_strip(hls::stream<apui_qw>& outStream, int mod_count){
    for (int i = 0; i < modules[mod_count].num_clusters; ++i) {
    DO_PRAGMA(HLS loop_tripcount min = 1 max = 16384)
    DO_PRAGMA(HLS pipeline II=2)
        apui_qw buf = 0;

        // calculate logical layer
        ap_uint<GHITZ_W1_LAST_bits> t1;
        if (i == modules[mod_count].num_clusters - 1) // If this strip hit is last, then LAST = 1
            t1 = 1;
        else
            t1 = 0;

        //Julian adds an extra GHITZ_LAST_bits
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits> t2 = (t1, (ap_uint<GHITZ_W1_LYR_bits>)(strip_hits[i].layer));
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits + GHITZ_W1_LAST_bits + GHITZ_W1_RAD_bits> t3 = (t2, ((ap_uint<GHITZ_W1_RAD_bits>)(strip_hits[i].r * (ap_ufixed<GHITZ_W1_RAD_apfix + 1, GHITZ_W1_RAD_apfix + 1>)GHITZ_W1_RAD_mf)));
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits + GHITZ_W1_LAST_bits + GHITZ_W1_RAD_bits + GHITZ_W1_PHI_bits> t4 = (t3, ((ap_int<GHITZ_W1_PHI_bits>)(strip_hits[i].phi * (ap_ufixed<GHITZ_W1_PHI_apfix + 1, GHITZ_W1_PHI_apfix + 1>)GHITZ_W1_PHI_mf)));
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits + GHITZ_W1_LAST_bits + GHITZ_W1_RAD_bits + GHITZ_W1_PHI_bits + GHITZ_W1_Z_bits> t5 = (t4, ((ap_int<GHITZ_W1_Z_bits>)(strip_hits[i].z * (ap_ufixed<GHITZ_W1_Z_apfix + 1, GHITZ_W1_Z_apfix + 1>)GHITZ_W1_Z_mf)));
        buf = (t5, (ap_uint<GHITZ_W1_ROW_bits + GHITZ_W1_SPARE_bits>)0); // Sets row and spare bits to zero

        outStream << buf;

        // Second word
        // Cluster1 and Cluster2 haven't been defined, so I'll leave it as zero. 

        ap_uint<GHITZ_W2_CLUSTER1_bits> t6 = (ap_uint<GHITZ_W2_CLUSTER1_bits>)0;
        ap_uint<GHITZ_W2_CLUSTER1_bits + GHITZ_W2_CLUSTER2_bits> t7 = (t6, 0);

        buf = (t7, (ap_uint<GHITZ_W2_SPARE_bits>)0);

        outStream << buf;
        // Set all spare bits to one to easily differentiate between SP and PIX cluster in test-bench
        //if (!hits_sp[i].unmatched)
        //    buf = (t5, (ap_uint<GHITZ_ROW_bits + GHITZ_SPARE_bits>)0xf);
    #if DEBUGOUTPUT
            XXX(" Logical layer: %d\n", (unsigned int)strip_hits[i].layer);
            XXX(" R: %f ", (double)strip_hits[i].r);
            XXX(" = %d\n", (unsigned int)(strip_hits[i].r * (ap_ufixed<GHITZ_W1_RAD_apfix + 1,GHITZ_W1_RAD_apfix + 1>)GHITZ_W1_RAD_mf));
            XXX(" global Phi: %f ", (double)strip_hits[i].phi);
            XXX(" = %d\n", (unsigned int)(strip_hits[i].phi * (ap_ufixed<GHITZ_W1_PHI_apfix + 1,GHITZ_W1_PHI_apfix + 1>)GHITZ_W1_PHI_mf));
            XXX(" Z: %f ", (double)hits_sp[i].z);
            XXX(" = %d\n\n", (int)(hits_sp[i].z * (ap_ufixed<GHITZ_W1_Z_apfix + 1,GHITZ_W1_Z_apfix + 1>)GHITZ_W1_Z_mf));
    #endif
        outStream << buf;
    }
}

void inline encode_ghitz_pix(hls::stream<apui_qw>& outStream, int mod_count){
    for (int i = 0; i < modules[mod_count].num_clusters; ++i) {
    DO_PRAGMA(HLS loop_tripcount min = 1 max = 16384)
        apui_qw buf = 0;

        // calculate logical layer
        ap_uint<GHITZ_W1_LAST_bits> t1;
        // if (hits_pix_count == 0 && i == hits_sp_count - 1)
        if (i == modules[mod_count].num_clusters - 1) // Filling in LAST field. If LAST = 1, then this hit is the last cluster in the module. 
            t1 = 1;
        else
            t1 = 0;


        //Julian adds an extra GHITZ_LAST_bits
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits> t2 = (t1, (ap_uint<GHITZ_W1_LYR_bits>)(pixel_hits[i].layer));
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits + GHITZ_W1_LAST_bits + GHITZ_W1_RAD_bits> t3 = (t2, ((ap_uint<GHITZ_W1_RAD_bits>)(pixel_hits[i].r * (ap_ufixed<GHITZ_W1_RAD_apfix + 1, GHITZ_W1_RAD_apfix + 1>)GHITZ_W1_RAD_mf)));
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits + GHITZ_W1_LAST_bits + GHITZ_W1_RAD_bits + GHITZ_W1_PHI_bits> t4 = (t3, ((ap_int<GHITZ_W1_PHI_bits>)(pixel_hits[i].phi * (ap_ufixed<GHITZ_W1_PHI_apfix + 1, GHITZ_W1_PHI_apfix + 1>)GHITZ_W1_PHI_mf)));
        ap_uint<GHITZ_W1_LAST_bits + GHITZ_W1_LYR_bits + GHITZ_W1_LAST_bits + GHITZ_W1_RAD_bits + GHITZ_W1_PHI_bits + GHITZ_W1_Z_bits> t5 = (t4, ((ap_int<GHITZ_W1_Z_bits>)(pixel_hits[i].z * (ap_ufixed<GHITZ_W1_Z_apfix + 1, GHITZ_W1_Z_apfix + 1>)GHITZ_W1_Z_mf)));
        buf = (t5, (ap_uint<GHITZ_W1_ROW_bits + GHITZ_W1_SPARE_bits>)0); // Sets row and spare bits to zero

        outStream << buf;

        // Second GHITZ word

        // Cluster1 and Cluster2 haven't been defined, so I'll leave it as zero. 

        ap_uint<GHITZ_W2_CLUSTER1_bits> t6 = (ap_uint<GHITZ_W2_CLUSTER1_bits>)0;
        ap_uint<GHITZ_W2_CLUSTER1_bits + GHITZ_W2_CLUSTER2_bits> t7 = (t6, (ap_uint<GHITZ_W2_CLUSTER2_bits>)0);

        buf = (t7, (ap_uint<GHITZ_W2_SPARE_bits>)0);

        outStream << buf;

#if DEBUGOUTPUT
        XXX(" Logical layer: %d\n", (unsigned int)pixel_hits[i].layer);
        XXX(" R: %f ", (double)pixel_hits[i].r);
        XXX(" = %d\n", (unsigned int)(pixel_hits[i].r * (ap_ufixed<GHITZ_W1_RAD_apfix + 1,GHITZ_W1_RAD_apfix + 1>)GHITZ_W1_RAD_mf));
        XXX(" global Phi: %f ", (double)pixel_hits[i].phi);
        XXX(" = %d\n", (unsigned int)(pixel_hits[i].phi * (ap_ufixed<GHITZ_W1_PHI_apfix + 1,GHITZ_W1_PHI_apfix + 1>)GHITZ_W1_PHI_mf));
        XXX(" Z: %f ", (double)pixel_hits[i].z);
        XXX(" = %d\n\n", (int)(pixel_hits[i].z * (ap_ufixed<GHITZ_W1_Z_apfix + 1,GHITZ_W1_Z_apfix + 1>)GHITZ_W1_Z_mf));
#endif
    }
}



void inline encode_event_footer(hls::stream<apui_qw>& outStream)
{
    apui_qw buf = 0;

    // First quad word
    buf = (apui_qw)ev.efoot.flag << EVT_FTR_W1_FLAG_lsb;
    buf |= (apui_qw)ev.efoot.spare << EVT_FTR_W1_SPARE_lsb;
    buf |= (apui_qw)ev.efoot.hdr_crc; //No shifting
    outStream << buf;

    // Second quad word
    outStream << ev.efoot.error_flags;

    // Third quad word
    buf = (apui_qw)ev.efoot.word_count << EVT_FTR_W3_WORD_COUNT_lsb;
    buf |= (apui_qw)ev.efoot.crc << EVT_FTR_W3_CRC_lsb;
    outStream << buf;
}


void encode_event(hls::stream<apui_qw>& outStream)
{
    encode_event_header(outStream);
    for (int i=0; i < module_count; ++i){
        encode_module_header(outStream, i);
        encode_ghitz(outStream, i);
    }
    encode_event_footer(outStream);
}

// read event data from global memory
void read_event(apui_qw *in, hls::stream<apui_qw> &inStream, unsigned int vSize)
{
mem_read:
    for (unsigned int i = 0; i < vSize; ++i) {
    DO_PRAGMA(HLS loop_tripcount min = EVENT_MINSIZE max = EVENT_MAXSIZE)
        inStream << in[i];
    }
}

// write event back to global memory
void write_event(apui_qw *out, hls::stream<apui_qw> &outStream, unsigned int vSize)
{
mem_write:
    for (unsigned int i = 0; i < vSize; ++i) {
    DO_PRAGMA(HLS loop_tripcount min = EVENT_MINSIZE max = EVENT_MAXSIZE)
        out[i] = outStream.read();
    }
}

void readdecode_event(apui_qw *in, hls::stream<apui_qw> &inStream, unsigned int vSize)
{
    #pragma HLS dataflow
    read_event(in, inStream, vSize);
    decode_event(inStream, vSize);
}

void encodewrite_event(apui_qw *out, hls::stream<apui_qw> &outStream, unsigned int vSize)
{
    #pragma HLS dataflow
    encode_event(outStream);
    write_event(out, outStream, vSize);
}


extern "C" {
void dut(apui_qw *in, apui_qw *out, unsigned int vSize)
{
    static hls::stream<apui_qw> inStream("inStream");
    static hls::stream<apui_qw> outStream("outStream");
    DO_PRAGMA(HLS INTERFACE m_axi port = in bundle = gmem0 depth = EVENT_MAXSIZE)
    DO_PRAGMA(HLS INTERFACE m_axi port = out bundle = gmem0 depth = EVENT_MAXSIZE)

    readdecode_event(in, inStream, vSize);

    // unsigned int n_sp = make_spacepoints();
    // Count the number of words for each module header and words from GHITZ
    // Note that GHITZ has two words
    // int count_words = module_count + 2*(hits_pix_count + hits_strip_count);

    // set correct word count
    ev.efoot.word_count = EVT_HDR_LWORDS + module_count + 2*(hits_pix_count + hits_strip_count);

    if (module_count + 2*(hits_pix_count + hits_strip_count)) encodewrite_event(out, outStream, ev.efoot.word_count);
}
}
