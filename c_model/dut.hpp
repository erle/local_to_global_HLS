#ifndef _DUT_H_
#define _DUT_H_

#include <ap_int.h>
#include <hls_stream.h>

#include "dataformat.hpp"
// #include "athena_dump_table.hpp"

#define PRAGMA_SUB(x) _Pragma (#x)
#define DO_PRAGMA(x) PRAGMA_SUB(x)
#define SELECTBITS(len, startbit) (((1ULL << len) - 1ULL) << startbit)

static event ev;
// zone, stave, side, hit
static pixel_cluster pixel_hits[4096];
static strip_cluster strip_hits[4096];
static module_ghitz modules[4096];
// static hit hits[4096];
// static sphit hits_sp[16384];
static ap_uint<15> hits_pix_count = 0;
static ap_uint<15> hits_strip_count = 0;
static ap_uint<15> hits_count = 0;
static ap_uint<15> module_count = 0;

// helper functions
void inline read_qword(apui_qw *buf, hls::stream<apui_qw> &inStream, unsigned int *i)
{
    *buf = inStream.read();
    ++*i;
}

void calculate_crc();

// decoder functions for different data structures
void decode_event(hls::stream<apui_qw> &inStream, unsigned int vSize);
void inline decode_event_footer(hls::stream<apui_qw> &inStream, unsigned int *i, apui_qw buf);
void inline decode_event_header(hls::stream<apui_qw> &inStream, unsigned int *i);
ap_uint<FLAG_bits> decode_module(hls::stream<apui_qw> &inStream, unsigned int *i, apui_qw buf);

// encoder functions for different data structures
void encode_event(hls::stream<apui_qw>& outStream);
void inline encode_event_footer(hls::stream<apui_qw>& outStream);
void inline encode_event_header(hls::stream<apui_qw>& outStream);
void encode_module_header(hls::stream<apui_qw>& outStream, int mod_count);
void inline encode_ghitz(hls::stream<apui_qw>& outStream, int mod_count);
void inline encode_ghitz_strip(hls::stream<apui_qw>& outStream, int mod_count);
void inline encode_ghitz_pix(hls::stream<apui_qw>& outStream, int mod_count);



// I/O
void read_event(apui_qw *in, hls::stream<apui_qw> &inStream, unsigned int vSize);
void write_event(apui_qw *out, hls::stream<apui_qw> &outStream, unsigned int vSize);

// main functions for device unter test
extern "C" {
void dut(apui_qw *in, apui_qw *out, unsigned int vSize);
}
void readdecode_event(apui_qw *in, hls::stream<apui_qw> &inStream, unsigned int vSize);
void encodewrite_event(apui_qw *out, hls::stream<apui_qw> &outStream, unsigned int vSize);

#endif
