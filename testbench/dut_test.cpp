#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include <ap_int.h>
#include <ap_fixed.h>

#include "../c_model/dataformat.hpp"
#include "../c_model/dut.hpp"
#include "../c_model/bitfield_definitions.h"

// #define MAX_EVENTS 7
#define MAX_EVENTS 1

int main(int argc, char* argv[])
{
    ap_uint<64> in[MAX_EVENTS][EVENT_MAXSIZE], out[MAX_EVENTS][EVENT_MAXSIZE], res[MAX_EVENTS][EVENT_MAXSIZE];
    FILE *fp;
    int test_fail = 0;
    int footer = 0;
    size_t ret;
    uint8_t buf;
    unsigned int vSize[MAX_EVENTS];
    unsigned int vSize_copy[MAX_EVENTS];
    unsigned int resSize[MAX_EVENTS];
    int n_events_in = -1;
    int n_events_res = -1;
    struct timespec start_cpu, start_wall, end_cpu, end_wall;
    double cputime, walltime;

    printf("Running test.\n");
    printf("Read in input testvectors.\n");
    //fp = fopen("./testvector_ghitzv1_nosp.bin", "rb");
    // fp = fopen("./testvector_ghitzv1_mu200_nosp.bin", "rb");
    fp = fopen("./pixel_cluster_output.bin", "rb");
    if (!fp) {
        perror("fopen() failed");
        return EXIT_FAILURE;
    }

    while (1) {
        if (n_events_in < 0) {
            in[0][0] = 0;
            out[0][0] = 0;
        } else {
            in[n_events_in][vSize[n_events_in]] = 0;
            out[n_events_in][vSize[n_events_in]] = 0;
        }
        for (int i = (sizeof in[0][0]) * 8 - 8; i >= 0; i -= 8) {
            ret = fread(&buf, sizeof buf, 1, fp);
            if (ret == 1) {
                if (buf == 0xab && i == (sizeof in[0][0]) * 8 - 8) {
                    if (n_events_in >= 0) printf("Event %d has %d 64bit words\n", n_events_in, vSize[n_events_in]);
                    ++n_events_in;
                    vSize[n_events_in] = 0;
                    printf("Reading in testvector for event %d, setting vSize[%d] = %d\n", n_events_in, n_events_in, vSize[n_events_in]);
                }
                if (n_events_in == MAX_EVENTS) goto close_file;
                in[n_events_in][vSize[n_events_in]] |= (ap_uint<64>)buf << i;
            } else {
                if (feof(fp)) {
                    printf("Event %d has %d 64bit words\n", n_events_in, vSize[n_events_in]);
                    printf("EOF reached; testvector read in.\n");
                    goto close_file;
                } else if (ferror(fp)) {
                    perror("Error reading testvector file");
                    return EXIT_FAILURE;
                }
            }
        }

        ++vSize[n_events_in];
        if (vSize[n_events_in] == EVENT_MAXSIZE) break;
    }
close_file:
    fclose(fp);
    printf("Finished obtaining input testvectors.\n\n");

    for (int i = 0; i <= n_events_in && i < MAX_EVENTS; ++i) vSize_copy[i] = vSize[i];
    for (int i = 0; i <= n_events_in && i < MAX_EVENTS; ++i) printf("vSize[%d] = %d\n", i, vSize[i]);

    printf("Read in result testvectors.\n");
    //fp = fopen("./testvector_ghitzv1_sp.bin", "rb");
    // fp = fopen("./testvector_ghitzv1_mu200_sp.bin", "rb");
    fp = fopen("./pixelL2G_output.bin", "rb");
    if (!fp) {
        perror("fopen() failed");
        return EXIT_FAILURE;
    }

    while (1) {
        if (n_events_res < 0)
            res[0][0] = 0;
        else
            res[n_events_res][resSize[n_events_res]] = 0;
        for (int i = (sizeof res[0][0]) * 8 - 8; i >= 0; i -= 8) {
            ret = fread(&buf, sizeof buf, 1, fp);
            if (ret == 1) {
                if (buf == 0xab && i == (sizeof res[0][0]) * 8 - 8) {
                    if (n_events_res >= 0) printf("Event %d has %d 64bit words\n", n_events_res, resSize[n_events_res]);
                    ++n_events_res;
                    resSize[n_events_res] = 0;
                    printf("Reading in testvector for event %d\n", n_events_res);
                }
                if (n_events_res == MAX_EVENTS) goto close_resfile;
                res[n_events_res][resSize[n_events_res]] |= (ap_uint<64>)buf << i;
            } else {
                if (feof(fp)) {
                    printf("Event %d has %d 64bit words\n", n_events_res, resSize[n_events_res]);
                    printf("EOF reached; testvector read in.\n");
                    goto close_resfile;
                } else if (ferror(fp)) {
                    perror("Error reading testvector file");
                    return EXIT_FAILURE;
                }
            }
        }

        ++resSize[n_events_res];
        if (resSize[n_events_res] == EVENT_MAXSIZE) break;
    }
close_resfile:
    for (int i = 0; i <= n_events_in && i < MAX_EVENTS; ++i) printf("vSize[%d] = %d\n", i, vSize[i]);
    for (int i = 0; i <= n_events_in && i < MAX_EVENTS; ++i) printf("vSize_copy[%d] = %d\n", i, vSize_copy[i]);
    fclose(fp);
    printf("Finished obtaining result testvectors.\n\n");

    printf("Running kernel.\n");
    //printf("Running kernel: input testvector size = %d quad words\n", vSize);
    //printf("Running kernel: output testvector size = %d quad words\n", resSize);
    /* printf("Firstly: dumping testvector:\n");
    for (int n = 0; n < vSize[0]; ++n) {
        printf("%2d: ", n);
        for (int i = (sizeof in[0][0]) * 8 - 1; i >= 0; --i) {
            unsigned char byte = (in[0][n] >> i) & 1;
            printf("%u", byte);
            if (!(i % 8)) printf(" ");
        }
        printf("\n");
    } */

    for (int i = 0; i <= n_events_in && i < MAX_EVENTS; ++i) printf("vSize[%d] = %d\n", i, vSize[i]);
    clock_gettime(CLOCK_MONOTONIC, &start_wall);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_cpu);
    for (int i = 0; i <= n_events_in && i < MAX_EVENTS; ++i) {
        printf("Running kernel: Event %d\n", i);
        dut(in[i], out[i], vSize[i]);
        printf("Finished event %d\n", i);
    }
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_cpu);
    clock_gettime(CLOCK_MONOTONIC, &end_wall);

    cputime = (double)end_cpu.tv_sec - (double)start_cpu.tv_sec + 1e-9 * ((double)end_cpu.tv_nsec - (double)start_cpu.tv_nsec);
    walltime = (double)end_wall.tv_sec - (double)start_wall.tv_sec + 1e-9 * ((double)end_wall.tv_nsec - (double)start_wall.tv_nsec);
    printf("Kernel finished running.\nCPU time [s] = %f\nWall time [s] = %f\n", cputime, walltime);
    /* printf("Dumping result:\n");
    for (int n = 0; n < resSize[0]; ++n) {
        printf("%2d: ", n);
        for (int i = (sizeof out[0][0]) * 8 - 1; i >= 0; --i) {
            unsigned char byte = (out[0][n] >> i) & 1;
            printf("%u", byte);
            if (!(i % 8)) printf(" ");
        }
        printf("\n");
    } */

    for (int n = 0; n <= n_events_res && n < MAX_EVENTS; ++n) {
        printf("Testing output for event %d\n", n);
        footer = 0;
        int outcount = 0;
        for (int i = EVT_HDR_LWORDS; i < resSize[n]; ++i) {
            if (((res[n][i] & SELECTBITS(FLAG_bits, FLAG_lsb)) >> FLAG_lsb) == EVT_FTR_FLAG) break;
            ++outcount;
        }
        //quicksort(out[n], EVT_HDR_LWORDS, outcount - 1);
        //quicksort(res[n], EVT_HDR_LWORDS, outcount - 1);
        //for (int i = 0; i < resSize[n]; ++i) {

        // Loop through the event data
        for (int i = EVT_HDR_LWORDS; i < outcount; ++i ) {
            // Check for module header
            if (((res[n][i] & SELECTBITS(FLAG_bits, FLAG_lsb)) >> FLAG_lsb) == M_HDR_FLAG) {
                printf("Module header detected at position %d\n", i);
                // Process the module header here if needed
                continue;
            }

        // Julian didn't use module headers. He only had event header, event footer and global hits. 
            int fail = 0;

            if (i >= EVT_HDR_LWORDS) {
                apuf_rad r_apf[2];
                apf_gphi gphi_apf[2];
                apf_z z_apf[2];
                unsigned int layer[2];

                layer[0] = (ap_uint<GHITZ_W1_LYR_bits>)((res[n][i] & SELECTBITS(GHITZ_W1_LYR_bits, GHITZ_W1_LYR_lsb)) >> GHITZ_W1_LYR_lsb);
                layer[1] = (ap_uint<GHITZ_W1_LYR_bits>)((out[n][i] & SELECTBITS(GHITZ_W1_LYR_bits, GHITZ_W1_LYR_lsb)) >> GHITZ_W1_LYR_lsb);
                r_apf[0] = ((ap_uint<GHITZ_W1_RAD_bits>)((res[n][i] & SELECTBITS(GHITZ_W1_RAD_bits, GHITZ_W1_RAD_lsb)) >> GHITZ_W1_RAD_lsb)) / GHITZ_W1_RAD_mf;
                r_apf[1] = ((ap_uint<GHITZ_W1_RAD_bits>)((out[n][i] & SELECTBITS(GHITZ_W1_RAD_bits, GHITZ_W1_RAD_lsb)) >> GHITZ_W1_RAD_lsb)) / GHITZ_W1_RAD_mf;
                gphi_apf[0] = ((ap_int<GHITZ_W1_PHI_bits>)((res[n][i] & SELECTBITS(GHITZ_W1_PHI_bits, GHITZ_W1_PHI_lsb)) >> GHITZ_W1_PHI_lsb)) / GHITZ_W1_PHI_mf;
                gphi_apf[1] = ((ap_int<GHITZ_W1_PHI_bits>)((out[n][i] & SELECTBITS(GHITZ_W1_PHI_bits, GHITZ_W1_PHI_lsb)) >> GHITZ_W1_PHI_lsb)) / GHITZ_W1_PHI_mf;
                z_apf[0] = ((ap_int<GHITZ_W1_Z_bits>)((res[n][i] & SELECTBITS(GHITZ_W1_Z_bits, GHITZ_W1_Z_lsb)) >> GHITZ_W1_Z_lsb)) / GHITZ_W1_Z_mf;
                z_apf[1] = ((ap_int<GHITZ_W1_Z_bits>)((out[n][i] & SELECTBITS(GHITZ_W1_Z_bits, GHITZ_W1_Z_lsb)) >> GHITZ_W1_Z_lsb)) / GHITZ_W1_Z_mf;

                float r[] = { r_apf[0], r_apf[1] };
                float gphi[] = { gphi_apf[0], gphi_apf[1] };
                float z[] = { z_apf[0], z_apf[1] };

                printf("CSV: %d,%f,%f,%f,%d,%f,%f,%f\n", layer[0], r[0], gphi[0], z[0], layer[1], r[1], gphi[1], z[1]);
                //Prints out lines of CSV that runs over the for loop

            }

        }
        printf("Finished event %d\n", n);
        }


    if (test_fail) {
        printf("\nTest failed.\n");
        return EXIT_FAILURE;
    }

    printf("Test passed.\n");
    return EXIT_SUCCESS;
}
